import org.gradle.plugins.ide.idea.model.IdeaLanguageLevel

plugins {
    java
    idea
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

idea {
    project {
        languageLevel = IdeaLanguageLevel("11")
        targetBytecodeVersion = JavaVersion.VERSION_11
    }
}

object Versions {
    val spring = "5.1.7.RELEASE"
    val springData = "2.1.8.RELEASE"
    val junit = "5.4.2"
    val junitPlatform = "1.4.2"
    val mockito = "2.27.0"
}

allprojects {
    group = "ru.x5.sm"
    version = "0.1-SNAPSHOT"
}

subprojects {
    apply(plugin = "java")

    val nexusUser: String by project
    val nexusPassword: String by project
    val nexusUrl: String by project

    repositories {
        maven {
            credentials {
                username = nexusUser
                password = nexusPassword
            }
            setUrl(nexusUrl)
        }
        mavenCentral()
    }


    dependencies {
        implementation("javax.annotation:javax.annotation-api:1.3.2")

        implementation("org.apache.logging.log4j:log4j-api:2.11.2")
        implementation("org.apache.logging.log4j:log4j-core:2.11.2")
        implementation("org.slf4j:slf4j-nop:1.7.26")

        implementation("commons-io:commons-io:2.6")
        implementation("org.apache.commons:commons-lang3:3.9")

        implementation("org.springframework:spring-core:${Versions.spring}")
        implementation("org.springframework:spring-context:${Versions.spring}")
        implementation("org.springframework:spring-jms:${Versions.spring}")
        implementation("org.springframework:spring-webflux:${Versions.spring}")
        implementation("org.springframework:spring-websocket:${Versions.spring}")
        implementation("org.springframework.data:spring-data-jpa:${Versions.springData}")
        implementation("org.hibernate:hibernate-core:5.4.2.Final")
        implementation("org.modelmapper:modelmapper:2.3.4")
        implementation("io.projectreactor.netty:reactor-netty:0.8.8.RELEASE")
        implementation("org.hibernate.validator:hibernate-validator:6.0.16.Final")
        implementation("javax.el:javax.el-api:3.0.0")
        implementation("org.glassfish.web:javax.el:2.2.6")

        implementation("org.springframework.security:spring-security-core:5.1.5.RELEASE")
        implementation("org.springframework.security:spring-security-web:5.1.5.RELEASE")
        implementation("org.springframework.security:spring-security-config:5.1.5.RELEASE")
        implementation("org.springframework.ldap:spring-ldap-core:2.3.2.RELEASE")
        implementation("io.jsonwebtoken:jjwt-jackson:0.10.6")
        implementation("io.jsonwebtoken:jjwt-api:0.10.6")
        implementation("io.jsonwebtoken:jjwt-impl:0.10.6")
        implementation("com.google.guava:guava:27.1-jre")

        testRuntime("org.junit.jupiter:junit-jupiter-engine:${Versions.junit}")
        testImplementation("org.junit.jupiter:junit-jupiter-api:${Versions.junit}")
        testImplementation("org.junit.jupiter:junit-jupiter-params:${Versions.junit}")
        testImplementation("org.mockito:mockito-junit-jupiter:${Versions.mockito}")
        testImplementation("org.mockito:mockito-core:${Versions.mockito}")
        testImplementation("org.springframework:spring-test:${Versions.spring}")
        testImplementation("org.junit.vintage:junit-vintage-engine:${Versions.junit}")
    }
}