plugins {
    java
    application
}

application {
    mainClassName = "ru.x5.sm.application.Application"
    applicationName = "storemanager_server"
    applicationDefaultJvmArgs = listOf(
            "-Dlog4j.configurationFile=/usr/local/gkretail/sm-web-server/configs/log4j2.xml",
            "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=0.0.0.0:4444")
}

distributions {
    main {
        baseName = "sm-web-server"
        version = ""
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":communication"))
    implementation(project(":database"))
    implementation(project(":job"))
}

task<Exec>("docker"){
    workingDir(projectDir)
    commandLine("docker build -t os-registry.x5.ru/sm-web:${version.toString().toLowerCase()} .")
}