package ru.x5.sm.application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.config.JmsConfig;
import ru.x5.sm.communication.config.LdapConfig;
import ru.x5.sm.communication.config.SecurityConfig;
import ru.x5.sm.database.config.DatabaseConfig;
import ru.x5.sm.job.config.JobConfig;

@Configuration
@Import({CommunicationConfig.class,
    SecurityConfig.class,
    JmsConfig.class,
    JobConfig.class,
    DatabaseConfig.class,
    LdapConfig.class})
public class ApplicationConfig {

}
