package ru.x5.sm.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ru.x5.sm.application.config.ApplicationConfig;

public class Application {
  private static final Logger LOG = LogManager.getLogger();

  public static void main(String[] args) {
    LOG.info("Initializing application");

    try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
      context.register(ApplicationConfig.class);
      context.refresh();
      LOG.info("Application context started");

      while (!Thread.currentThread().isInterrupted()) {
        Thread.sleep(1000);
      }

    } catch (Exception e) {
      LOG.error("", e);
    } finally {
      LOG.info("Application stopped");
    }
  }
}
