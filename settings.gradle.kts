rootProject.name = "sm-web"
include("application")
include("core")
include("database")
include("structure")
include("job")
include("communication")