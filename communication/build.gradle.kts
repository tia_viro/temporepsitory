
plugins {
    java
}

dependencies {
    implementation(project(":core"))
    implementation("org.synchronoss.cloud:nio-multipart-parser:1.1.0")
    implementation("org.springframework.kafka","spring-kafka" ,"2.2.8.RELEASE")
    implementation("io.projectreactor.kafka", "reactor-kafka", "1.1.1.RELEASE")
    implementation("commons-validator","commons-validator","1.6")

    testImplementation("org.mockito:mockito-core:2.7.22")
    testImplementation("org.junit.vintage:junit-vintage-engine:5.4.2")
    testImplementation("io.projectreactor:reactor-test:3.2.11.RELEASE")
}