package ru.x5.sm.communication.dto.store;

import ru.x5.sm.dto.job.status.TaskStepStatus;

public class FrontStepDto {
  public static FrontStepDto of(String name, String shortMessage, TaskStepStatus stepStatus) {
    final FrontStepDto storeStepsDto = new FrontStepDto();
    storeStepsDto.setName(name);
    storeStepsDto.setShortMessage(shortMessage);
    storeStepsDto.setStatus(stepStatus);
    return storeStepsDto;
  }

  private String name;
  private String shortMessage;
  private TaskStepStatus status;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortMessage() {
    return shortMessage;
  }

  public void setShortMessage(String shortMessage) {
    this.shortMessage = shortMessage;
  }

  public TaskStepStatus getStatus() {
    return status;
  }

  public void setStatus(TaskStepStatus status) {
    this.status = status;
  }
}
