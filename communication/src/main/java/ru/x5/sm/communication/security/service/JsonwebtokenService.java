package ru.x5.sm.communication.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JsonwebtokenService implements SecurityTokenService {
  private final static String SECURITY_TOKEN_KEY = "security.jwt.secret_key";
  private final static String SECURITY_TOKEN_EXPIRATION_TIME = "security.jwt.expiration_time";

  private final String secretKey;
  private final String expirationTime;

  public JsonwebtokenService(Environment env) {
    this.secretKey = env.getProperty(SECURITY_TOKEN_KEY, DEFAULT_SECRET_KEY);
    this.expirationTime = env.getProperty(SECURITY_TOKEN_EXPIRATION_TIME, DEFAULT_EXPIRATION_TIME);
  }

  @Override
  public String generateToken(String username, List<String> roles) {
    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    return Jwts.builder()
            .setHeaderParam("type", "JWT")
            .setSubject(username)
            .claim("roles", String.join(",", new HashSet<>(roles)))
            .signWith(new SecretKeySpec(DatatypeConverter.parseBase64Binary(secretKey), signatureAlgorithm.getJcaName()), signatureAlgorithm)
            .compact();
  }

  @Override
  public Boolean validateToken(String token) {
//    Проверка не истекло ли время действия токена.
//    return getAllClaimsFromToken(token)
//            .getExpiration()
//            .before(new Date());
    return true;
  }


  @Override
  public Claims getAllClaimsFromToken(String token) {
    return Jwts.parser()
            .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey))
            .parseClaimsJws(token).getBody();
  }

  @Override
  public String getUsernameFromToken(String token) {
    return getAllClaimsFromToken(token).getSubject();
  }

  @Override
  public List<GrantedAuthority> getAuthoritiesFromToken(String token) {
    return Arrays.stream(getAllClaimsFromToken(token).get("roles", String.class).split(","))
            .map(role -> (GrantedAuthority) () -> role)
            .collect(Collectors.toList());
  }
}
