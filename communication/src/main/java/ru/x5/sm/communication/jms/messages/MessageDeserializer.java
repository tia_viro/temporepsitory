package ru.x5.sm.communication.jms.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class MessageDeserializer<T extends Message> implements Deserializer<T> {

  private static final Log logger = LogFactory.getLog(MessageDeserializer.class);

  @Override
  public void configure(Map<String, ?> map, boolean b) {

  }

  @Override
  public T deserialize(String s, byte[] bytes) {
    ObjectMapper mapper = new ObjectMapper();
    T msg = null;
    try {
      msg = (T) mapper.readValue(bytes, MessageType.getTypeMap().get(s));
    } catch (Exception e) {
      logger.error("Error deserialize object", e);
    }
    return msg;
  }

  @Override
  public void close() {

  }
}
