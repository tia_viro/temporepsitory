package ru.x5.sm.communication.dto.store;

import ru.x5.sm.communication.dto.XrgFrontDto;

import java.util.ArrayList;
import java.util.List;

public class FrontStationDto extends XrgFrontDto {
  private String name;
  private String systemType;
  private List<FrontTaskDto> tasks;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSystemType() {
    return systemType;
  }

  public void setSystemType(String systemType) {
    this.systemType = systemType;
  }

  public List<FrontTaskDto> getTasks() {
    return tasks == null ? new ArrayList<>() : tasks;
  }

  public void setTasks(List<FrontTaskDto> tasks) {
    this.tasks = tasks;
  }
}
