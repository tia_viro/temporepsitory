package ru.x5.sm.communication.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;
import ru.x5.sm.communication.dto.ErrorMessageDto;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.job.task.processor.JobProcessor;

import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@Component
public class JobsRunnerHandler {
    private static final Logger LOG = LogManager.getLogger();

    private final JobProcessor jobProcessor;
    private final Validator validator;

    public JobsRunnerHandler(JobProcessor jobProcessor, Validator validator) {
        this.jobProcessor = jobProcessor;
        this.validator = validator;
    }

    public Mono<ServerResponse> runJob(ServerRequest request) {
        return request.bodyToMono(JobRunnerDto.class)
                .doOnNext(this::validate)
                .handle(this::processJob)
                .flatMap(job -> ServerResponse.ok().build())
                .onErrorResume(e -> Mono.just(ErrorMessageDto.newBuilder()
                        .setStatus(HttpStatus.BAD_REQUEST)
                        .setMessage(e.getMessage()).build())
                            .flatMap(s -> ServerResponse.badRequest()
                                .contentType(APPLICATION_JSON_UTF8)
                                .syncBody(s)));
    }

    private void processJob(JobRunnerDto jobRunnerDto, SynchronousSink<JobRunnerDto> synchronousSink) {
        try {
            jobProcessor.process(jobRunnerDto);
            synchronousSink.next(jobRunnerDto);
        } catch (Exception e) {
            synchronousSink.error(e.getCause());
        }
    }

    private void validate(JobRunnerDto jobRunnerDto) {
        Errors errors = new BeanPropertyBindingResult(jobRunnerDto, "jobRunnerRequest");
        validator.validate(jobRunnerDto, errors);
        if (errors.hasErrors()) {
            throw new ServerWebInputException(
                    String.format("Request did not passed validation. Errors count %d.  %s ",
                            errors.getErrorCount(), errors.getAllErrors()
                                    .stream()
                                    .map(ObjectError::toString)
                                    .collect(Collectors.joining("\n"))));
        }
    }
}
