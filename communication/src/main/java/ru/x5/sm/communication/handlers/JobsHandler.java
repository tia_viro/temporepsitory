package ru.x5.sm.communication.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.converters.JobDtoConverter;
import ru.x5.sm.communication.converters.SimpleJobDtoConverter;
import ru.x5.sm.communication.dto.job.FrontJobDto;
import ru.x5.sm.communication.dto.job.JobPeriodDto;
import ru.x5.sm.communication.dto.job.SimpleFrontJobDto;
import ru.x5.sm.communication.routes.JobRouter;
import ru.x5.sm.database.service.DaoJobService;
import ru.x5.sm.database.service.DaoUserService;
import ru.x5.sm.dto.filter.JobFilterDto;
import ru.x5.sm.dto.job.JobDto;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class JobsHandler {
  private static final Logger LOG = LogManager.getLogger();

  private DaoJobService databaseJobService;
  private DaoUserService userService;
  private Validator validator;
  private JobDtoConverter dtoConverter;
  private SimpleJobDtoConverter simpleJobDtoConverter;

  public JobsHandler(DaoJobService databaseJobService, DaoUserService userService,
                     Validator validator, JobDtoConverter dtoConverter, SimpleJobDtoConverter simpleJobDtoConverter) {
    this.databaseJobService = databaseJobService;
    this.userService = userService;
    this.validator = validator;
    this.dtoConverter = dtoConverter;
    this.simpleJobDtoConverter = simpleJobDtoConverter;
  }

  public Mono<ServerResponse> create(ServerRequest request) {
    return request
        .bodyToMono(FrontJobDto.class)
        .doOnNext(this::validate)
        .flatMap(dtoConverter::convertToCoreMono)
        .flatMap(this::save)
        .flatMap(j ->
            ServerResponse.created(URI.create(JobRouter.baseUri + "/" + j.getId()))
                .contentType(APPLICATION_JSON_UTF8).syncBody(j))
        .doOnError(e -> LOG.error(e.getMessage()));
    //TODO проработать исключения
  }

  private Mono<JobDto> save(JobDto job) {
    // обогащение задания логином пользователя из секъюрити контекста
    // и сохранение в БД
    return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getName)
        .flatMap(login -> userService.findByLogin(login))
        .doOnNext(job::setAuthor)
        .flatMap(userDto -> databaseJobService.saveJob(job));
  }

  public Mono<ServerResponse> findById(ServerRequest request) {
    Mono<ServerResponse> response;
    try {
      Long jobId = Long.valueOf(request.pathVariable("id"));
      response = databaseJobService.findById(jobId)
          .map(dtoConverter::convertToFront)
          .flatMap(j -> ServerResponse.ok()
              .contentType(APPLICATION_JSON_UTF8)
              .syncBody(j))
          .switchIfEmpty(ServerResponse.notFound().build())
          .onErrorResume(e -> Mono.just(String.format("Error: %s", e.getMessage()))
              .flatMap(s -> ServerResponse.badRequest()
                  .contentType(TEXT_PLAIN)
                  .syncBody(s)));
    } catch (NumberFormatException e) {
      response = ServerResponse.badRequest()
          .body(BodyInserters.fromObject("Wrong id format, should be Long"));
    }
    return response;
  }

  private void validate(FrontJobDto job) {
    if (job.getPeriod() == null){
      JobPeriodDto jobPeriodDto = new JobPeriodDto();
      jobPeriodDto.setFrom(LocalDateTime.of(2019, Month.JUNE, 25, 23, 00, 00));
      jobPeriodDto.setTo(LocalDateTime.of(9999, Month.JUNE, 25, 23, 00, 00));
      job.setPeriod(jobPeriodDto);
    }

    LOG.debug("job class: " + (job.getClass()));
    Errors errors = new BeanPropertyBindingResult(job, "jobRequest");
    validator.validate(job, errors);
    if (errors.hasErrors()) {
      throw new ServerWebInputException(
          String.format("Request did not passed validation. Errors count %d.  %s ",
              errors.getErrorCount(), errors.getAllErrors()
                  .stream()
                  .map(ObjectError::toString)
                  .collect(Collectors.joining("\n"))));
    }
  }
}
