package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ru.x5.sm.communication.dto.TemplateVersionsByNodeGroupsDto;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonTypeName("update")
public class UpdateFrontJobDto extends FrontJobDto {

  @NotNull
  private List<TemplateVersionsByNodeGroupsDto> templateVersions;

  private Long total;

  public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public List<TemplateVersionsByNodeGroupsDto> getTemplateVersions() {
    return templateVersions;
  }

  public void setTemplateVersions(List<TemplateVersionsByNodeGroupsDto> templateVersions) {
    this.templateVersions = templateVersions;
  }
}
