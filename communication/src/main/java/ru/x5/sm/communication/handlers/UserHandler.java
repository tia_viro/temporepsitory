package ru.x5.sm.communication.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.security.service.UserLdapServiceImpl;
import ru.x5.sm.database.service.DaoUserService;
import ru.x5.sm.dto.users.UserDto;
import ru.x5.sm.ldap.service.UserLdapService;

import java.util.function.Function;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
public class UserHandler {
  private static final Logger LOG = LogManager.getLogger();

  private DaoUserService userService;
  private UserLdapService userLdapService;

  public UserHandler() {
  }

  public Mono<ServerResponse> findAll(ServerRequest request) {
    final String loginStarted = request.queryParam("loginStarted").orElse("");

    final Flux<UserDto> users = userService.findAll(loginStarted);

    return ServerResponse
        .ok()
        .contentType(APPLICATION_JSON)
        .body(users, UserDto.class);
  }

  public Mono<ServerResponse> findByLogin(ServerRequest request) {
    final String userLogin = request.pathVariable("login");

    return ServerResponse
        .ok()
        .contentType(APPLICATION_JSON)
        .body(userLdapService.findUserByLogin(userLogin), UserDto.class);
  }

  public Mono<ServerResponse> findCurrent(ServerRequest request) {
    return ReactiveSecurityContextHolder.getContext()
            .map(SecurityContext::getAuthentication)
            .map(Authentication::getName)
            .flatMap((Function<String, Mono<UserDto>>) login -> userLdapService.findUserByLogin(login))
            .flatMap(userDto -> ServerResponse
                    .ok()
                    .contentType(APPLICATION_JSON).body(Mono.just(userDto), UserDto.class));
  }

  @Autowired
  public void setUserService(DaoUserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setUserLdapService(UserLdapService userLdapService) {
    this.userLdapService = userLdapService;
  }
}
