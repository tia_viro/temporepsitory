package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.NodesHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.method;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class StructureNodesRouter {
  @Bean
  public RouterFunction<ServerResponse> nodeStructureRoutes(NodesHandler handler) {
    return nest(path("/api/v1/structurenodes/{id}"),
        nest(accept(APPLICATION_JSON),
            route(method(HttpMethod.GET), handler::getStructureNodes)
        ));
  }
}
