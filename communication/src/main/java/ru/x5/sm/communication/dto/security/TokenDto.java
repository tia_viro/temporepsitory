package ru.x5.sm.communication.dto.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenDto {
    @JsonProperty("access_token")
    private String accessToken;

    public TokenDto(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public TokenDto() {
    }
}
