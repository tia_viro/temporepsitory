package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.JobsStatusHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class JobsStatusRouter {
  public static final String BASE_URL =  "/api/v1/jobs_status";
  @Bean
  public RouterFunction<ServerResponse> jobsStatusRoutes(JobsStatusHandler handler) {
    final RouterFunction<ServerResponse> routes =
        nest(accept(APPLICATION_JSON_UTF8),
            route(GET("/"), handler::findAllJobStatuses));

    return nest(path(BASE_URL), routes);
  }
}
