package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.JobStoresHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class JobStoresRouter {
  public static final String BASE_URL = "/api/v1/jobstores";
  @Bean
  public RouterFunction<ServerResponse> jobStoreRoutes(JobStoresHandler handler) {
    final RouterFunction<ServerResponse> jobStoreRoutes =
        nest(accept(APPLICATION_JSON_UTF8),
            route(GET(""), handler::getStoresInJob)
                .andRoute(GET("/steps/{id}"), handler::getStepsInJob)
                .andRoute(GET("/statuses"), handler::getSatuses));

    return nest(path(BASE_URL), jobStoreRoutes);
  }
}
