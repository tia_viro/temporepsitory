package ru.x5.sm.communication.config;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.security.AuthenticationManager;
import ru.x5.sm.communication.security.SecurityContextRepository;

import java.util.Map;
import java.util.Set;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Import(SecurityRouterPathsConfig.class)
@ComponentScan({"ru.x5.sm.communication.security"})
public class SecurityConfig {
  private AuthenticationManager authenticationManager;
  private SecurityContextRepository securityContextRepository;
  private Map<String, Set<String>> securityPathsMap;

  @Bean
  public SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {
     http.cors()
           .and()
             .exceptionHandling()
             .authenticationEntryPoint((swe, e) -> Mono.fromRunnable(() -> swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED)))
             .accessDeniedHandler((swe, e) -> Mono.fromRunnable(() -> swe.getResponse().setStatusCode(HttpStatus.FORBIDDEN)))
           .and()
             .csrf().disable()
             .authenticationManager(authenticationManager)
             .securityContextRepository(securityContextRepository)
             .authorizeExchange()
             .pathMatchers("/login").permitAll();
     securityPathsMap.forEach((path, auth) -> dynamicPathMatchers(http, path, auth));
     http.authorizeExchange().anyExchange().authenticated();
     return http.build();
  }

  private void dynamicPathMatchers(ServerHttpSecurity http, String path, Set<String> authorities){
    if(authorities.size() == 1){
      authorities.forEach(auth -> http.authorizeExchange().pathMatchers(path).hasAuthority(auth));
    } else if(authorities.size() > 1){
      http.authorizeExchange().pathMatchers(path).access(manager(authorities));
    }
  }

  private ReactiveAuthorizationManager<AuthorizationContext> manager(Set<String> auth){
    return (authentication, object) -> authentication
            .map(authentic -> authentic.getAuthorities().stream().anyMatch(authority -> auth.contains(authority.getAuthority())))
            .map(AuthorizationDecision::new);
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    final CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(ImmutableList.of("*"));
    configuration.setAllowedMethods(ImmutableList.of("*"));
    configuration.setAllowedHeaders(ImmutableList.of("*"));
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }

  @Autowired
  public void setAuthenticationManager(AuthenticationManager authenticationManager) {
    this.authenticationManager = authenticationManager;
  }

  @Autowired
  public void setSecurityContextRepository(SecurityContextRepository securityContextRepository) {
    this.securityContextRepository = securityContextRepository;
  }

  @Autowired
  public void setSecurityPathsMap(Map<String, Set<String>> securityPathsMap) {
    this.securityPathsMap = securityPathsMap;
  }
}
