package ru.x5.sm.communication.security.service;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Set;

public interface SecurityTokenService {
  String DEFAULT_SECRET_KEY = "RfUjWnZr4u7x!A%D*G-KaPdSgVkYp2s5v8y/B?E(H+MbQeThWmZq4t6w9z$C&F)J";
  String DEFAULT_EXPIRATION_TIME = "300";

  String generateToken(String username, List<String> roles);

  Boolean validateToken(String token);

  Claims getAllClaimsFromToken(String token);

  String getUsernameFromToken(String token);

  List<GrantedAuthority> getAuthoritiesFromToken(String token);
}
