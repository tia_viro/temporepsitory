package ru.x5.sm.communication.jms.listeners;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import reactor.core.scheduler.Schedulers;
import reactor.kafka.receiver.ReceiverRecord;
import ru.x5.sm.communication.jms.messages.TaskStatusMessage;
import ru.x5.sm.communication.websocket.WebSocketSessionHandler;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.database.service.DaoTaskStatusService;
import ru.x5.sm.dto.job.status.TaskStatusDto;
import ru.x5.sm.dto.job.task.TaskDto;

public class TaskStatusListener {
  private static final Logger LOG = LogManager.getLogger();

  private final DaoTaskService taskService;
  private final DaoTaskStatusService taskStatusService;
  private final WebSocketSessionHandler webSocketSessionHandler;

  public TaskStatusListener(DaoTaskService taskService, DaoTaskStatusService taskStatusService,
      WebSocketSessionHandler webSocketSessionHandler) {
    this.taskService = taskService;
    this.taskStatusService = taskStatusService;
    this.webSocketSessionHandler = webSocketSessionHandler;
  }

  public void getMessage(TaskStatusMessage msg) {
    taskStatusService.save(msg.getStatusDto())
        .doOnError(Throwable::printStackTrace)
        .flatMap(status -> taskService.findTask(status.getTaskId()))
        .map(task -> processTaskStatus(task, msg.getStatusDto()))
        .doOnError(Throwable::printStackTrace)
        .flatMap(webSocketSessionHandler::sendTask)
        .subscribeOn(Schedulers.parallel())
        .subscribe();
  }

  private TaskDto processTaskStatus(TaskDto task, TaskStatusDto taskStatus) {
    LOG.info("process task {}, status {}", task, taskStatus.getState());
    task.setTaskState(taskStatus.getState());
    task.setTaskStateTime(taskStatus.getTime());
    task.setTaskStateMessage(
        StringUtils.isEmpty(taskStatus.getStepName()) && StringUtils.isNotEmpty(taskStatus.getShortMessage()) ?
            taskStatus.getShortMessage() : "");
    return task;
  }


  public void getMessage(ReceiverRecord<Integer, TaskStatusMessage> integerTaskStatusMessageReceiverRecord) {
    getMessage(integerTaskStatusMessageReceiverRecord.value());
  }
}
