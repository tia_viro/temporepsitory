package ru.x5.sm.communication.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import javax.naming.Context;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource(value = "file:${config.dir:config}/ldap.properties", ignoreResourceNotFound = true)
public class LdapConfig {
  private static final String CONNECT_TIMEOUT_LDAP_PROPERTY = "com.sun.jndi.ldap.connect.timeout";
  private static final String CONNECT_POOL_LDAP_PROPERTY = "com.sun.jndi.ldap.connect.pool";

  public static final String LDAP_LOGIN_NAME_MAPPING = "operator.login.attribute.username";
  public static final String LDAP_LOGIN_BASE_MAPPING = "operator.login.base";
  public static final String LDAP_LOGIN_AUTHORITY_MATCHER = "operator.login.authority.matcher";

  private static final String LDAP_CONNECT_URL ="ldap.connect.url:";
  private static final String LDAP_CONNECT_BASE ="ldap.connect.base";
  private static final String LDAP_CONNECT_USERNAME ="ldap.connect.username";
  private static final String LDAP_CONNECT_PASSWORD ="ldap.connect.password";
  private static final String LDAP_CONNECT_DOWNLOAD_AUTH_TYPE ="ldap.connect.download.authentication.type";
  private static final String LDAP_CONNECT_TIMEOUT ="ldap.connect.timeout";
  private static final String LDAP_CONNECT_POLL ="ldap.connect.pool";

  private String ldapUrl;
  private String base;
  private String username;
  private String password;
  private String downloadAuthType;
  private String connectTimeout;
  private String connectPool;

  public LdapConfig(Environment environment) {
    this.ldapUrl = environment.getProperty(LDAP_CONNECT_URL, "ldap://ldap.x5.ru:389/");
    this.base = environment.getProperty(LDAP_CONNECT_BASE, "DC=X5,DC=ru");
    this.username = environment.getProperty(LDAP_CONNECT_USERNAME, "srv.gk@x5.ru");
    this.password = environment.getProperty(LDAP_CONNECT_PASSWORD, "$Rj$%z%+Kj85mUr!");
    this.downloadAuthType = environment.getProperty(LDAP_CONNECT_DOWNLOAD_AUTH_TYPE, "simple");
    this.connectTimeout = environment.getProperty(LDAP_CONNECT_TIMEOUT, "10000");
    this.connectPool = environment.getProperty(LDAP_CONNECT_POLL, "false");
  }

  @Bean
  public LdapContextSource contextSource() {
    LdapContextSource contextSource = new LdapContextSource();
    contextSource.setUrl(ldapUrl);
    contextSource.setBase(base);
    contextSource.setUserDn(username);
    contextSource.setPassword(password);
    Map<String, Object> env = new HashMap<>();
    env.put(CONNECT_TIMEOUT_LDAP_PROPERTY, connectTimeout);
    env.put(CONNECT_POOL_LDAP_PROPERTY, connectPool);
    env.put(Context.SECURITY_AUTHENTICATION, downloadAuthType);
    contextSource.setBaseEnvironmentProperties(env);
    return contextSource;
  }

  @Bean
  public LdapTemplate ldapTemplate() {
    LdapTemplate ldapTemplate = new LdapTemplate(contextSource());
    ldapTemplate.setIgnorePartialResultException(true);
    return ldapTemplate;
  }

}
