package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.x5.sm.communication.dto.AuthorFrontDto;
import ru.x5.sm.communication.dto.XrgFrontDto;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeDesSerializer;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeSerializer;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = UpdateFrontJobDto.class, name = "update"),
    @JsonSubTypes.Type(value = RollbackFrontJobDto.class, name = "rollback"),
    @JsonSubTypes.Type(value = DownloadFrontJobDto.class, name = "download"),
    @JsonSubTypes.Type(value = RebootFrontJobDto.class, name = "reboot")
})
public class FrontJobDto extends XrgFrontDto {
  @Null
  private String status;

  @NotNull
  @Size(max = 255)
  private String name;

  @JsonProperty(value = "type")
  @NotNull
  private String type;

  @Size(max = 255)
  private String description;

  @Null
  private LocalDateTime created;

  @NotNull
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private JobPeriodDto period;

  @Null
  private AuthorFrontDto author;

  private Integer totalStores;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public JobPeriodDto getPeriod() {
    return period;
  }

  public void setPeriod(JobPeriodDto period) {
    this.period = period;
  }

  public AuthorFrontDto getAuthor() {
    return author;
  }

  public void setAuthor(AuthorFrontDto author) {
    this.author = author;
  }

  @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
  @JsonDeserialize(using = CustomLocalDateTimeDesSerializer.class)
  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public Integer getTotalStores() {
    return totalStores;
  }

  public void setTotalStores(int totalStores) {
    this.totalStores = totalStores;
  }

  public FrontJobDto() {
  }
}
