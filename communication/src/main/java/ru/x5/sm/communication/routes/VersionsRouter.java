package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.VersionsHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class VersionsRouter {
  @Bean
  public RouterFunction<ServerResponse> versionsRoutes(VersionsHandler handler) {

    final RouterFunction<ServerResponse> versionsRoutes =
        nest(accept(APPLICATION_JSON_UTF8),
            route(GET("/{id}"), handler::getVersionsByNodeId));

    return nest(path("/api/v1/nodeversions"), versionsRoutes);
  }
}
