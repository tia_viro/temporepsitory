package ru.x5.sm.communication.jms.messages;

import ru.x5.sm.dto.job.status.TaskStatusDto;

public class TaskStatusMessage extends Message {
  private TaskStatusDto statusDto;

  public TaskStatusDto getStatusDto() {
        return statusDto;
    }

  public void setStatusDto(TaskStatusDto statusDto) {
        this.statusDto = statusDto;
    }
}
