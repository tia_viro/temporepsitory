package ru.x5.sm.communication.handlers;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.dto.job.DownloadFilePathDto;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

@Component
public class FileUploadCheckHandler {
    private static final String NEXUS_URL = "communication.nexus.url";
    private static final String POSSIBLE_FILE_EXTENSIONS = "communication.job.download.extensions";

    private WebClient nexusWebClient;

    private final UrlValidator urlValidator;
    private final String nexusUrl;
    private final List<String> fileExtensions;

    public FileUploadCheckHandler(Environment environment, WebClient nexusWebClient, UrlValidator urlValidator) {
        this.nexusWebClient = nexusWebClient;
        this.nexusUrl = environment.getProperty(NEXUS_URL);
        this.fileExtensions = List.of(environment.getProperty(POSSIBLE_FILE_EXTENSIONS, "MD5,zip,rar,tar.gz").split(","));
        this.urlValidator = urlValidator;
    }

    public Mono<ServerResponse> checkFileForUpload(ServerRequest request) {
        return request.bodyToMono(DownloadFilePathDto.class)
                .flatMap(filePathDto -> Mono.just(filePathDto.getFilePath())
                        .filter(urlValidator::isValid)
                        .filter(filePath -> filePath.contains(nexusUrl))
                        .filter(filePath -> fileExtensions.stream().anyMatch(filePath::contains))
                        .map(filePath -> filePath.replace(nexusUrl, ""))
                        .flatMap(filePath -> nexusWebClient.get().uri(filePath).exchange()
                                .flatMap((Function<ClientResponse, Mono<MediaType>>) clientResponse -> Mono.justOrEmpty(clientResponse.headers().contentType()))
                                .flatMap(mediaType -> ServerResponse.ok().build())
                                .switchIfEmpty(ServerResponse.badRequest().build()))
                        .switchIfEmpty(ServerResponse.badRequest().build()))
                .switchIfEmpty(ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
    }
}
