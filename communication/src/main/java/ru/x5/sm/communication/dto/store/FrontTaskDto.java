package ru.x5.sm.communication.dto.store;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ru.x5.sm.communication.dto.XrgFrontDto;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeSerializer;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.type.TaskType;

import java.time.LocalDateTime;
import java.util.List;

public class FrontTaskDto extends XrgFrontDto {
  private Long jobId;
  private Long stationId;
  private TaskType type;
  private TaskState taskState;
  private List<FrontStepDto> steps;
  private String taskStateMessage;
  private LocalDateTime taskStateTime;

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public Long getStationId() {
    return stationId;
  }

  public void setStationId(Long stationId) {
    this.stationId = stationId;
  }

  public TaskType getType() {
    return type;
  }

  public void setType(TaskType type) {
    this.type = type;
  }

  public TaskState getTaskState() {
    return taskState;
  }

  public void setTaskState(TaskState taskState) {
    this.taskState = taskState;
  }

  public List<FrontStepDto> getSteps() {
    return steps;
  }

  public void setSteps(List<FrontStepDto> steps) {
    this.steps = steps;
  }

  @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
  public LocalDateTime getTaskStateTime() {
    return taskStateTime;
  }

  public void setTaskStateTime(LocalDateTime taskStateTime) {
    this.taskStateTime = taskStateTime;
  }

  public String getTaskStateMessage() {
    return taskStateMessage;
  }

  public void setTaskStateMessage(String taskStateMessage) {
    this.taskStateMessage = taskStateMessage;
  }
}