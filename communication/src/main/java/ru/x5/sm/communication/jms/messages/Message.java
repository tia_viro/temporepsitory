package ru.x5.sm.communication.jms.messages;

public abstract class Message {
  private MessageType type;
  private String message;

  public MessageType getType() {
        return type;
    }

  public void setType(MessageType type) {
        this.type = type;
    }

  public String getMessage() {
        return message;
    }

  public void setMessage(String message) {
        this.message = message;
    }

  @Override
  public String toString() {
    return "Message{" +
           ", type=" + type +
           ", message='" + message + '\'' +
           '}';
  }
}
