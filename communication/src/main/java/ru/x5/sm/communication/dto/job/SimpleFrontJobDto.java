package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.x5.sm.communication.dto.AuthorFrontDto;
import ru.x5.sm.communication.dto.XrgFrontDto;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeDesSerializer;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeSerializer;

import java.time.LocalDateTime;
import java.util.List;

public class SimpleFrontJobDto extends XrgFrontDto {
  private String status;
  private String type;
  private String name;
  private AuthorFrontDto author;
  private JobPeriodDto period;
  private LocalDateTime created;
  private List<String> targetNodes;

  public SimpleFrontJobDto() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AuthorFrontDto getAuthor() {
    return author;
  }

  public void setAuthor(AuthorFrontDto author) {
    this.author = author;
  }

  public JobPeriodDto getPeriod() {
    return period;
  }

  public void setPeriod(JobPeriodDto period) {
    this.period = period;
  }

  public List<String> getTargetNodes() {
    return targetNodes;
  }

  public void setTargetNodes(List<String> targetNodes) {
    this.targetNodes = targetNodes;
  }

  @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
  @JsonDeserialize(using = CustomLocalDateTimeDesSerializer.class)
  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }
}
