package ru.x5.sm.communication.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.dto.security.LoginDto;
import ru.x5.sm.ldap.service.UserLdapService;

import java.util.stream.Collectors;

@Component
public class LoginHandler {
  private Validator validator;
  private UserLdapService userLdapService;

  public Mono<ServerResponse> login(ServerRequest request) {
    return request.bodyToMono(LoginDto.class)
      .doOnNext(this::validate)
      .flatMap(loginDto -> userLdapService.authenticate(loginDto.getUsername(), loginDto.getPassword()));
  }

  private void validate(LoginDto jobRunnerDto) {
    Errors errors = new BeanPropertyBindingResult(jobRunnerDto, "loginRequest");
    validator.validate(jobRunnerDto, errors);
    if (errors.hasErrors()) {
      throw new ServerWebInputException(
        String.format("Request did not passed validation. Errors count %d.  %s ",
          errors.getErrorCount(), errors.getAllErrors()
            .stream()
            .map(ObjectError::toString)
            .collect(Collectors.joining("\n"))));
        }
    }

  @Autowired
  public void setValidator(Validator validator) {
    this.validator = validator;
  }

  @Autowired
  public void setUserLdapService(UserLdapService userLdapService) {
    this.userLdapService = userLdapService;
  }

}
