package ru.x5.sm.communication.jms.messages;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public enum MessageType {
  XRG_TASK_STATUS;

  private static final Map<String, Class<?>> typeMap = Collections.unmodifiableMap(initTypeMap());

  private static Map<String, Class<?>> initTypeMap() {
    Map<String, Class<?>> typeMap = new LinkedHashMap<>();
    typeMap.put(XRG_TASK_STATUS.name(), TaskStatusMessage.class);
    return typeMap;
  }

  public static Map<String, Class<?>> getTypeMap() {
        return typeMap;
    }
}
