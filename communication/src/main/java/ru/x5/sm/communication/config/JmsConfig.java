package ru.x5.sm.communication.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;
import ru.x5.sm.communication.jms.listeners.TaskStatusListener;
import ru.x5.sm.communication.jms.messages.MessageDeserializer;
import ru.x5.sm.communication.jms.messages.TaskStatusMessage;
import ru.x5.sm.communication.websocket.WebSocketSessionHandler;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.database.service.DaoTaskStatusService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//@Configuration
//@ComponentScan(basePackages = "ru.x5.sm.communication.jms")
public class JmsConfig {
//  private static final String BROKER_URL_PROPERTY = "broker.url";
//  private static final String DEFAULT_BROKER_URL = "localhost:9092";
//  private static final String GROUP_ID_CONFIG_PROPERTY = "group.id";
//  private static final String DEFAULT_GROUP_ID_CONFIG = "test-consumer-group";
//  private static final String SM_RECEIVER_USER = "sm.receiver.user";
//  private static final String SM_RECEIVER_USER_PASS = "sm.receiver.user.pass";
//
//
//  @Bean
//  public TaskStatusListener taskStatusListener(DaoTaskService taskService, DaoTaskStatusService taskStatusService,
//                                               WebSocketSessionHandler webSocketSessionHandler, Environment env){
//    TaskStatusListener taskStatusListener = new TaskStatusListener(taskService,taskStatusService,webSocketSessionHandler);
//    ReceiverOptions<Integer, TaskStatusMessage> receiverOptions = ReceiverOptions.<Integer, TaskStatusMessage>create(consumerConfigs(env))
//            .subscription(Collections.singleton("XRG_TASK_STATUS"));
//    Flux<ReceiverRecord<Integer, TaskStatusMessage>> inboundFlux =
//            KafkaReceiver.create(receiverOptions)
//                    .receive();
//    inboundFlux.subscribe(taskStatusListener::getMessage);
//    return taskStatusListener;
//  }
//
//
//  public Map<String, Object> consumerConfigs(Environment env) {
//    Map<String, Object> props = new HashMap<>();
//    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, env.getProperty(BROKER_URL_PROPERTY, DEFAULT_BROKER_URL));
//    props.put(ConsumerConfig.GROUP_ID_CONFIG, env.getProperty(GROUP_ID_CONFIG_PROPERTY, DEFAULT_GROUP_ID_CONFIG));
//    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, MessageDeserializer.class);
//    return props;
//  }
}
