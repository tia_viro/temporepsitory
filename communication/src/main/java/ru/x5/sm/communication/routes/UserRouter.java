package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.UserHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class UserRouter {
  @Bean
  public RouterFunction<ServerResponse> userRoutes(UserHandler handler) {

    final RouterFunction<ServerResponse> routes =
        nest(accept(APPLICATION_JSON_UTF8),
            route(GET("/"), handler::findAll)
                    .andRoute(GET("/current"), handler::findCurrent)
                .andRoute(GET("/login/{login}"), handler::findByLogin));

    return nest(path("/api/v1/users"), routes);
  }
}
