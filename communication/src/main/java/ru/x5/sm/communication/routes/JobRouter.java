package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.JobsHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class JobRouter {
  public static final String baseUri = "/api/v1/jobs";
  @Bean
  public RouterFunction<ServerResponse> jobRoutes(JobsHandler handler) {

    final RouterFunction<ServerResponse> jobRoutes =
        nest(accept(APPLICATION_JSON_UTF8),
            route(GET("/"), handler::findJobs)
                .andRoute(GET("/{id}"), handler::findById)
                .andRoute(POST("/"), handler::create));

    return nest(path(baseUri), jobRoutes);
  }
}
