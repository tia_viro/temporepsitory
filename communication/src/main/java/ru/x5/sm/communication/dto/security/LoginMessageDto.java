package ru.x5.sm.communication.dto.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginMessageDto {
    @JsonProperty("message")
    private String message;

    public LoginMessageDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
