package ru.x5.sm.communication.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.config.LdapConfig;
import ru.x5.sm.communication.dto.security.TokenDto;
import ru.x5.sm.communication.security.mapper.UserAttributesMapper;
import ru.x5.sm.database.service.DaoUserService;
import ru.x5.sm.dto.users.UserDto;
import ru.x5.sm.ldap.service.UserLdapService;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Service
@PropertySources({
        @PropertySource(value = "file:${config.dir:config}/ldap.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:security_path.properties", ignoreResourceNotFound = true)
})
public class UserLdapServiceImpl implements UserLdapService {

  private LdapTemplate ldapTemplate;
  private SecurityTokenService tokenService;
  private UserAttributesMapper userAttributesMapper;
  private DaoUserService userService;

  private final String loginNameMapping;
  private final String loginBaseMapping;
  private final List<String> operatorAuthorityMatcher;

  public UserLdapServiceImpl(Environment environment) {
    this.loginNameMapping = environment.getProperty(LdapConfig.LDAP_LOGIN_NAME_MAPPING, "mailNickname");
    this.loginBaseMapping = environment.getProperty(LdapConfig.LDAP_LOGIN_BASE_MAPPING, "ou=Users,ou=Central,ou=Main");
    this.operatorAuthorityMatcher = List.of(environment.getProperty(LdapConfig.LDAP_LOGIN_AUTHORITY_MATCHER, "C-GK_TEAM").split(","));
  }

  @Override
  public Mono<ServerResponse> authenticate(String username, String password) {
    return Mono.just(ldapTemplate.authenticate(loginBaseMapping, String.format("%s=%s", loginNameMapping, username), password))
            .filter(authenticateResult -> authenticateResult)
            .flatMap((Function<Boolean, Mono<ServerResponse>>) authenticateResult -> findUserByLogin(username)
                    .filter(userDto -> operatorAuthorityMatcher.contains("**") || userDto.getGroups().stream().distinct().anyMatch(operatorAuthorityMatcher::contains))
                    .doOnNext(userDto -> userService.existsByLogin(userDto.getLogin())
                            .filter(userExists -> !userExists)
                            .subscribe(aBoolean -> userService.save(userDto)))
                    .flatMap(userDto -> ServerResponse.ok()
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .body(Mono.just(new TokenDto(tokenService.generateToken(userDto.getLogin(), userDto.getGroups()))), TokenDto.class))
                    .switchIfEmpty(ServerResponse.status(HttpStatus.FORBIDDEN).build()))
            .switchIfEmpty(ServerResponse.status(HttpStatus.UNAUTHORIZED).build());
  }

  @Override
  public Mono<UserDto> findUserByLogin(String login) {
    return Mono.justOrEmpty(ldapTemplate.search(query().base(loginBaseMapping).where(loginNameMapping).is(login), userAttributesMapper).stream().findFirst());
  }

  @Autowired
  public void setLdapTemplate(LdapTemplate ldapTemplate) {
    this.ldapTemplate = ldapTemplate;
  }

  @Autowired
  public void setTokenService(SecurityTokenService tokenService) {
    this.tokenService = tokenService;
  }

  @Autowired
  public void setUserAttributesMapper(UserAttributesMapper userAttributesMapper) {
    this.userAttributesMapper = userAttributesMapper;
  }

  @Autowired
  public void setUserService(DaoUserService userService) {
    this.userService = userService;
  }
}
