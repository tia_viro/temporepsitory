package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DownloadFilePathDto {
    @JsonProperty("file_path")
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
