package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Set;

@JsonTypeName("download")
public class DownloadFrontJobDto extends FrontJobDto {
  @JsonProperty("bandWidth")
  private Integer bandwidth;
  private String downloadTo;
  @JsonProperty("pathToFile")
  private Set<String> pathsToFiles;
  private Set<String> targetSCNodes;
  private Boolean isNexusLoad;

  public Integer getBandwidth() {
    return bandwidth;
  }

  public void setBandwidth(Integer bandwidth) {
    this.bandwidth = bandwidth;
  }

  public Set<String> getPathsToFiles() {
    return pathsToFiles;
  }

  public void setPathsToFiles(Set<String> pathsToFiles) {
    this.pathsToFiles = pathsToFiles;
  }

  public Set<String> getTargetSCNodes() {
    return targetSCNodes;
  }

  public void setTargetSCNodes(Set<String> targetSCNodes) {
    this.targetSCNodes = targetSCNodes;
  }

  public String getDownloadTo() {
    return downloadTo;
  }

  public void setDownloadTo(String downloadTo) {
    this.downloadTo = downloadTo;
  }

  public Boolean getNexusLoad() {
    return isNexusLoad;
  }

  public void setNexusLoad(Boolean nexusLoad) {
    isNexusLoad = nexusLoad;
  }
}
