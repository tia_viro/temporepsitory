package ru.x5.sm.communication.config;

import org.apache.commons.validator.routines.UrlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.Base64Utils;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.reactive.DispatcherHandler;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.WebSocketService;
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import org.springframework.web.reactive.socket.server.upgrade.ReactorNettyRequestUpgradeStrategy;
import org.springframework.web.server.adapter.WebHttpHandlerBuilder;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import javax.validation.Validator;
import java.util.Collections;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

@Configuration
@ComponentScan({"ru.x5.sm.communication.routes",
        "ru.x5.sm.communication.converters",
        "ru.x5.sm.communication.handlers",
        "ru.x5.sm.communication.websocket",
        "ru.x5.sm.communication.serializers"})
@EnableWebFlux
@EnableScheduling
@PropertySource(value = "file:${config.dir:config}/communication.properties", ignoreResourceNotFound = true)
public class CommunicationConfig implements WebFluxConfigurer {
  private static final Logger LOG = LogManager.getLogger();

  private static final String NEXUS_URL = "communication.nexus.url";
  private static final String NEXUS_USERNAME = "communication.nexus.username";
  private static final String NEXUS_PASSWORD = "communication.nexus.password";


  private static final String REST_HOST = "communication.rest.host";
  private static final String REST_PORT = "communication.rest.port";
  private static final String DEFAULT_PORT = "8010";

  private final int port;
  private final String hostAddress;
  private final String nexusUrl;
  private final String nexusUsername;
  private final String nexusPassword;

  private final WebSocketHandler webSocketHandler;

  @Autowired
  public CommunicationConfig(Environment env, WebSocketHandler webSocketHandler) {
    this.port = Integer.parseInt(env.getProperty(REST_PORT, DEFAULT_PORT));
    this.hostAddress = env.getProperty(REST_HOST, "localhost");
    this.webSocketHandler = webSocketHandler;
    this.nexusUrl = env.getProperty(NEXUS_URL);
    this.nexusUsername = env.getProperty(NEXUS_USERNAME);
    this.nexusPassword = env.getProperty(NEXUS_PASSWORD);
  }

  @Bean
  @DependsOn("handlerAdapter")
  public DispatcherHandler webHandler(ApplicationContext applicationContext) {
    return new DispatcherHandler(applicationContext);
  }

  @Bean
  @DependsOn("webHandler")
  public DisposableServer initServer(ApplicationContext applicationContext) {
    HttpHandler httpHandler = WebHttpHandlerBuilder.applicationContext(applicationContext).build();

    ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(httpHandler);
    return HttpServer.create()
        .handle(adapter)
        .host(hostAddress)
        .port(port)
        .bind()
        .block();
  }

  @Bean
  public HandlerMapping webSocketHandlerMapping() {
    Map<String, WebSocketHandler> map = Collections.singletonMap("/api/v1/tasks", webSocketHandler);

    SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
    handlerMapping.setOrder(-1);
    handlerMapping.setUrlMap(map);
    return handlerMapping;
  }

  @Bean
  public WebSocketHandlerAdapter handlerAdapter() {
    return new WebSocketHandlerAdapter(webSocketService());
  }

  @Bean
  public WebSocketService webSocketService() {
    ReactorNettyRequestUpgradeStrategy strategy = new ReactorNettyRequestUpgradeStrategy();
    return new HandshakeWebSocketService(strategy);
  }

  @Bean
  public WebClient nexusWebClient(){
    return WebClient
            .builder()
            .baseUrl(nexusUrl)
            .defaultHeader(HttpHeaders.AUTHORIZATION, String.format("Basic %s", Base64Utils
                    .encodeToString(String.format("%s:%s", nexusUsername, nexusPassword).getBytes(UTF_8))))
            .build();
  }

  @Bean
  public Validator validator() {
    return new LocalValidatorFactoryBean();
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Bean
  public UrlValidator urlValidator(){
    return new UrlValidator();
  }
}
