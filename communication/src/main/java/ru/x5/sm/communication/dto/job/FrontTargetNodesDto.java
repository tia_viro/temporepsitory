package ru.x5.sm.communication.dto.job;

import ru.x5.sm.communication.dto.store.FrontStoreDto;

import java.util.List;

public class FrontTargetNodesDto {
  private Integer totalStores;
  private Integer done;

  public Integer getTotalStores() {
    return totalStores;
  }

  public void setTotalStores(Integer totalStores) {
    this.totalStores = totalStores;
  }

  public Integer getDone() {
    return done;
  }

  public void setDone(Integer done) {
    this.done = done;
  }
}
