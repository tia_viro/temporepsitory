package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.handlers.JobsRunnerHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Component
public class JobsRunnerRouter {

  @Bean
  public RouterFunction<ServerResponse> jobRunnerRouter(JobsRunnerHandler handler) {
    final RouterFunction<ServerResponse> routes =
      nest(accept(APPLICATION_JSON_UTF8),
        route(POST("/"), handler::runJob));
    return nest(path("/api/v1/jobs/runner"), routes);
  }
}
