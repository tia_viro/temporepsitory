package ru.x5.sm.communication.dto.store;

import ru.x5.sm.communication.dto.XrgFrontDto;

import java.util.List;

public class FrontStoreDto extends XrgFrontDto {
  private String name;

  private List<FrontStationDto> stations;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<FrontStationDto> getStations() {
    return stations;
  }

  public void setStations(List<FrontStationDto> stations) {
    this.stations = stations;
  }
}

