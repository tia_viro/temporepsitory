package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.x5.sm.dto.job.SelectedNodeDto;

import java.util.List;

@JsonTypeName("rollback")
public class RollbackFrontJobDto extends FrontJobDto {
  private List<SelectedNodeDto> selectedNodes;

  public List<SelectedNodeDto> getSelectedNodes() {
    return selectedNodes;
  }

  public void setSelectedNodes(List<SelectedNodeDto> selectedNodes) {
    this.selectedNodes = selectedNodes;
  }
}
