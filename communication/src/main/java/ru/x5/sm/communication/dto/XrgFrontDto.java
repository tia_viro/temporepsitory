package ru.x5.sm.communication.dto;

import javax.validation.constraints.Null;
import java.util.Objects;

public class XrgFrontDto {
  @Null
  protected Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof XrgFrontDto)) return false;
    XrgFrontDto that = (XrgFrontDto) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
