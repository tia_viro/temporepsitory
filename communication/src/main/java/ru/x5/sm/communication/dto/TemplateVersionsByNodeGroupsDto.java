package ru.x5.sm.communication.dto;

import java.util.List;

public class TemplateVersionsByNodeGroupsDto {
  private List<String> nodes;
  private String source;
  private String target;

  public TemplateVersionsByNodeGroupsDto() {
  }

  public TemplateVersionsByNodeGroupsDto(List<String> nodes, String source, String target) {
    this.nodes = nodes;
    this.source = source;
    this.target = target;
  }

  public List<String> getNodes() {
    return nodes;
  }

  public void setNodes(List<String> nodes) {
    this.nodes = nodes;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }
}
