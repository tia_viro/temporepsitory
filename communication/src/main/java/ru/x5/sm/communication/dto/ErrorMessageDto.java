package ru.x5.sm.communication.dto;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ErrorMessageDto implements Serializable {
    private HttpStatus status;
    private String message;

    public static Builder newBuilder() {
        return new ErrorMessageDto().new Builder();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public class Builder {

        private Builder() {
        }

        public Builder setStatus(HttpStatus status) {
            ErrorMessageDto.this.status = status;
            return this;
        }

        public Builder setMessage(String message) {
            ErrorMessageDto.this.message = message;
            return this;
        }

        public ErrorMessageDto build() {
            return ErrorMessageDto.this;
        }

    }
}
