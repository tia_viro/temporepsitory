package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.JobsHandler;
import ru.x5.sm.communication.handlers.LoginHandler;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class LoginRouter {

  @Bean
  public RouterFunction<ServerResponse> loginRoute(LoginHandler handler) {
    final RouterFunction<ServerResponse> loginRoute =
      nest(accept(APPLICATION_JSON_UTF8),
        route(POST("/login"), handler::login));
    return nest(path("/"), loginRoute);
  }
}
