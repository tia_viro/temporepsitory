package ru.x5.sm.communication.security.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;
import ru.x5.sm.dto.users.UserDto;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Service
public class UserAttributesMapper implements AttributesMapper<UserDto> {
  private LdapTemplate ldapTemplate;

  @Override
  public UserDto mapFromAttributes(Attributes attributes) throws NamingException {
    UserDto user = new UserDto();
    user.setLogin((String) attributes.get("mailNickname").get());
    user.setFirstName((String) attributes.get("extensionAttribute2").get());
    user.setLastName((String) attributes.get("extensionAttribute1").get());
    user.setDepartment((String) attributes.get("department").get());
    user.setMobile((String) attributes.get("mobile").get());
    String distinguishedName = String.valueOf(attributes.get("distinguishedName").get());
    user.setGroups(ldapTemplate.search(query().where("member").is(distinguishedName),
      (AttributesMapper<String>) attributes1 -> String.valueOf(attributes1.get("cn").get())));
    return user;
  }

    @Autowired
  public void setLdapTemplate(LdapTemplate ldapTemplate) {
    this.ldapTemplate = ldapTemplate;
  }
}
