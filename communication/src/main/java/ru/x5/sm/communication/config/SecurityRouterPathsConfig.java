package ru.x5.sm.communication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Configuration
@PropertySource(value = "classpath:security_path.properties", ignoreResourceNotFound = true)
public class SecurityRouterPathsConfig {
  private static final String SECURITY_PATH_PROPERTY = "security.%s.path";
  private static final String SECURITY_AUTHORITY_PROPERTY = "security.%s.authority";

  private Environment environment;

  @Bean(name = "securityPathsMap")
  public Map<String, Set<String>> generateSecurityPathsWithRequiredAuthorities(){
    Map<String, Set<String>> securityPaths = new HashMap<>();
    for (int i = 1; i <= 100; i++){
      String path = environment.getProperty(String.format(SECURITY_PATH_PROPERTY, i));
      String auth = environment.getProperty(String.format(SECURITY_AUTHORITY_PROPERTY, i));
      if(Objects.nonNull(path) && Objects.nonNull(auth)){
        securityPaths.put(path, Set.of(auth.split(",")));
      }
    }
    return securityPaths;
  }

  @Autowired
  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }
}
