package ru.x5.sm.communication.routes;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.x5.sm.communication.config.CommunicationConfig;
import ru.x5.sm.communication.handlers.FileUploadCheckHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Import(CommunicationConfig.class)
public class FileUploadCheckRouter {
  @Bean
  public RouterFunction<ServerResponse> fileUploadCheckRoute(FileUploadCheckHandler handler){
    return route(RequestPredicates.POST("/api/v1/download/upload")
      .and(accept(MediaType.APPLICATION_JSON_UTF8)), handler::checkFileForUpload);
  }
}
