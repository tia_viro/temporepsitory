package ru.x5.sm.communication.dto.job;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeDesSerializer;
import ru.x5.sm.communication.serializers.CustomLocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class JobPeriodDto implements Serializable {
  private LocalDateTime from;
  private LocalDateTime to;

  @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
  @JsonDeserialize(using = CustomLocalDateTimeDesSerializer.class)
  public LocalDateTime getFrom() {
    return from;
  }

  public void setFrom(LocalDateTime from) {
    this.from = from;
  }

  @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
  @JsonDeserialize(using = CustomLocalDateTimeDesSerializer.class)
  public LocalDateTime getTo() {
    return to;
  }

  public void setTo(LocalDateTime to) {
    this.to = to;
  }

  public JobPeriodDto() {
  }

  public JobPeriodDto(LocalDateTime from, LocalDateTime to) {
    this.from = from;
    this.to = to;
  }
}
