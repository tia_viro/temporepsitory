package ru.x5.sm.communication.security;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.x5.sm.communication.security.service.SecurityTokenService;

import java.util.Collections;
import java.util.List;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {
  private final SecurityTokenService tokenService;

  public AuthenticationManager(SecurityTokenService tokenService) {
        this.tokenService = tokenService;
    }

  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    String authToken = authentication.getCredentials().toString();
    String username = "igor.tishik";
    List<GrantedAuthority> authorities = List.of((GrantedAuthority) () -> "C-GK_TEAM");
//    try{
//      username = tokenService.getUsernameFromToken(authToken);
//      authorities = tokenService.getAuthoritiesFromToken(authToken);}
//    catch (Exception e) {
//      username = null;
//      authorities = Collections.emptyList();
//    }
    if (username != null && !authorities.isEmpty() && tokenService.validateToken(authToken)){
      return Mono.just(new UsernamePasswordAuthenticationToken(
              username,null, authorities));
    } else {
      return Mono.empty();
    }
  }
}
