plugins {
    java
}

dependencies {
    implementation(project(":core"))
    implementation("io.projectreactor.netty:reactor-netty:0.8.8.RELEASE")
    implementation("javax.annotation:jsr250-api:1.0")

    testImplementation("com.squareup.okhttp3:mockwebserver:3.9.1")
}