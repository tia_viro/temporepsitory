package subscriber;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.UpdateTaskDto;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.service.TaskStateUpdateService;
import ru.x5.sm.job.subscriber.TaskSubscriber;
import ru.x5.sm.job.task.processor.TaskProcessor;
import ru.x5.sm.job.task.service.TaskCancelService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TaskSubscriberTest {
  @Mock
  private TaskProcessor taskProcessor;
  @Mock
  private TaskStateUpdateService taskStateUpdateService;
  @Mock
  private TaskCancelService cancelService;

  private TaskSubscriber taskSubscriber;
  private MockWebServer mockWebServer;

  private UpdateTaskDto task;
  private TaskProcessorData<UpdateTaskDto> taskProcessorData;

  @Before
  public void before() {
    mockWebServer = new MockWebServer();
    taskSubscriber = new TaskSubscriber(taskProcessor, taskStateUpdateService, cancelService, 2, 16);
    generateTestValues();
  }

  @AfterEach
  public void tearDown() throws IOException {
    mockWebServer.shutdown();
    generateTestValues();
  }

  @Test
  public void processSuccessfully() throws Exception {
    when(taskStateUpdateService.checkStatus(TaskState.CANCELED, task.getId()))
      .thenReturn(Mono.just(false));
    prepareResponse(response -> response.setResponseCode(200));
    taskSubscriber.process(taskProcessorData);
    RecordedRequest recordedRequest = mockWebServer.takeRequest();

    verify(taskStateUpdateService, times(2)).checkStatus(TaskState.CANCELED, task.getId());
    verify(taskStateUpdateService, times(1)).updateStatus(task, TaskState.SENT);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING_FAILED);
    verify(taskStateUpdateService, times(1)).updateStatus(task, TaskState.SENDING);
    verify(taskProcessor, never()).process(any());
    verify(cancelService, never()).cancelTask(any());

    assert recordedRequest.getRequestUrl().toString().equals(taskProcessorData.getUrl());
    assert !recordedRequest.getBody().toString().isEmpty();
  }

  @Test
  public void processMidCanceled() throws Exception {
    when(taskStateUpdateService.checkStatus(TaskState.CANCELED, task.getId()))
      .thenReturn(Mono.just(false))
      .thenReturn(Mono.just(true));
    prepareResponse(response -> response.setResponseCode(200));
    taskSubscriber.process(taskProcessorData);
    RecordedRequest recordedRequest = mockWebServer.takeRequest();

    verify(taskStateUpdateService, times(2)).checkStatus(TaskState.CANCELED, task.getId());
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENT);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING_FAILED);
    verify(taskStateUpdateService,  times(1)).updateStatus(task, TaskState.SENDING);
    verify(taskProcessor, never()).process(any());
    verify(cancelService, times(1)).cancelTask(any());

    assert recordedRequest.getRequestUrl().toString().equals(taskProcessorData.getUrl());
    assert !recordedRequest.getBody().toString().isEmpty();
  }

  @Test
  public void processMaxSendingTries() {
    when(taskStateUpdateService.checkStatus(TaskState.CANCELED, task.getId()))
      .thenReturn(Mono.just(false));
    taskProcessorData.getTaskDto().setFailCount(2);
    taskSubscriber.process(taskProcessorData);

    verify(taskStateUpdateService, times(1)).checkStatus(TaskState.CANCELED, task.getId());
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENT);
    verify(taskStateUpdateService, times(1)).updateStatus(task, TaskState.SENDING_FAILED);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING);
    verify(taskProcessor, never()).process(any());
    verify(cancelService, never()).cancelTask(any());
  }

  @Test
  public void processCanceledTask() {
    when(taskStateUpdateService.checkStatus(TaskState.CANCELED, task.getId()))
      .thenReturn(Mono.just(true));
    taskSubscriber.process(taskProcessorData);

    verify(taskStateUpdateService, times(1)).checkStatus(TaskState.CANCELED, task.getId());
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENT);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING_FAILED);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING);
    verify(taskProcessor, never()).process(any());
    verify(cancelService, never()).cancelTask(any());
  }

  @Test
  public void processWithBadResponse() throws Exception {
    when(taskStateUpdateService.checkStatus(TaskState.CANCELED, task.getId()))
      .thenReturn(Mono.just(false));
    prepareResponse(response -> response.setResponseCode(404));
    taskSubscriber.process(taskProcessorData);
    RecordedRequest recordedRequest = mockWebServer.takeRequest();

    verify(taskStateUpdateService, times(2)).checkStatus(TaskState.CANCELED, task.getId());
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENT);
    verify(taskStateUpdateService, never()).updateStatus(task, TaskState.SENDING_FAILED);
    verify(taskStateUpdateService, times(1)).updateStatus(task, TaskState.SENDING);
    verify(taskProcessor, times(1)).process(any());
    verify(cancelService, never()).cancelTask(any());

    assert recordedRequest.getRequestUrl().toString().equals(taskProcessorData.getUrl());
    assert !recordedRequest.getBody().toString().isEmpty();
  }

  private void prepareResponse(Consumer<MockResponse> consumer) {
    MockResponse response = new MockResponse();
    consumer.accept(response);
    mockWebServer.enqueue(response);
  }

  private void generateTestValues(){
    task = new UpdateTaskDto();
    task.setId(1L);
    task.setType(TaskType.UPDATE);
    task.setOldVersion("old_version");
    task.setNewVersion("new_version");
    task.setPackageName("R24-v12.08.34-SP53.zip");
    task.setValidFrom(LocalDateTime.parse("2018-10-09T08:00:00"));
    task.setValidTo(LocalDateTime.parse("2099-10-09T11:00:00"));
    taskProcessorData = new TaskProcessorData<>();
    taskProcessorData.setTaskDto(task);
    taskProcessorData.setUrl(mockWebServer.url("/").toString());
    taskProcessorData.setTaskURI("");
  }
}
