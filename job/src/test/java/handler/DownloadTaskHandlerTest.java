package handler;

import filler.TestFillerDataGenerator;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import reactor.core.publisher.Mono;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.DownloadJobDto;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.params.DownloadTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.handler.DownloadTaskHandler;
import ru.x5.sm.job.task.processor.TaskProcessor;

import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DownloadTaskHandlerTest {
  @Mock
  private DaoStationService stationService;
  @Mock
  private DaoTaskService taskService;
  @Mock
  private TaskProcessor taskProcessor;
  @Mock(answer = Answers.RETURNS_SMART_NULLS)
  private UpdateFileNameService updateFileNameService;
  @InjectMocks
  private DownloadTaskHandler downloadTaskHandler;

  private JobDto job;
  private DownloadJobDto downloadJobDto;
  private DownloadTaskUniqueParam taskCreationParam;
  private SMStationDto smStation;

  @Before
  public void before() {
    job = TestFillerDataGenerator.generateJobWithTargetNodes();
    downloadJobDto = TestFillerDataGenerator.generateDownloadJobWithTargetNodes();
    taskCreationParam = new DownloadTaskUniqueParam();
    taskCreationParam.setTemplateVersions(TestFillerDataGenerator.generateTemplateVersionsByNodeGroupsDtos());
    taskCreationParam.setPathToFile(Set.of("file"));
    smStation = TestFillerDataGenerator.generateSMStationDto();
  }

  @Test
  public void createTaskForDownloadJobSuccessfullyTest() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> captor = ArgumentCaptor.forClass(TaskProcessorData.class);
    when(stationService.findStationDtoByName(any())).thenReturn(Mono.just(smStation));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(downloadJobDto, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskProcessor, times(2)).process(captor.capture());

    assert captor.getValue().getTaskDto().getType().equals(TaskType.DOWNLOAD);
    assert captor.getValue().getUrl().equals("http://localhost:8080");
  }

  @Test
  public void createTaskForDownloadJobWithErrorTest() throws NotFoundException {
    ArgumentCaptor<TaskDto> captor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName(any())).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(downloadJobDto, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(2)).updateTask(captor.capture());

    assert captor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert captor.getValue().getTaskState().equals(TaskState.ERROR);
    assert captor.getValue().getType().equals(TaskType.DOWNLOAD);
  }

  @Test
  public void createTaskForDownloadJobMixedTest() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> successCaptor = ArgumentCaptor.forClass(TaskProcessorData.class);
    ArgumentCaptor<TaskDto> errorCaptor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName("firstNode")).thenReturn(Mono.just(smStation));
    when(stationService.findStationDtoByName("secondNode")).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(downloadJobDto, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(1)).updateTask(errorCaptor.capture());
    verify(taskProcessor, times(1)).process(successCaptor.capture());

    assert successCaptor.getValue().getTaskDto().getType().equals(TaskType.DOWNLOAD);
    assert successCaptor.getValue().getUrl().equals("http://localhost:8080");
    assert errorCaptor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getTaskState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getType().equals(TaskType.DOWNLOAD);
  }

  @Test
  public void createTaskForUpdateJobSuccessfullyTest() throws NotFoundException {
    job.setType(JobType.UPDATE);
    ArgumentCaptor<TaskProcessorData<?>> captor = ArgumentCaptor.forClass(TaskProcessorData.class);
    when(stationService.findStationDtoByName(any())).thenReturn(Mono.just(smStation));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskProcessor, times(2)).process(captor.capture());

    assert captor.getValue().getTaskDto().getType().equals(TaskType.DOWNLOAD);
    assert captor.getValue().getUrl().equals("http://localhost:8080");
  }

  @Test
  public void createTaskForUpdateJobWithErrorTest() throws NotFoundException {
    job.setType(JobType.UPDATE);
    ArgumentCaptor<TaskDto> captor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName(any())).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(2)).updateTask(captor.capture());

    assert captor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert captor.getValue().getTaskState().equals(TaskState.ERROR);
    assert captor.getValue().getType().equals(TaskType.DOWNLOAD);
  }

  @Test
  public void createTaskForUpdateJobMixedTest() throws NotFoundException {
    job.setType(JobType.UPDATE);
    ArgumentCaptor<TaskProcessorData<?>> successCaptor = ArgumentCaptor.forClass(TaskProcessorData.class);
    ArgumentCaptor<TaskDto> errorCaptor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName("firstNode")).thenReturn(Mono.just(smStation));
    when(stationService.findStationDtoByName("secondNode")).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    downloadTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(1)).updateTask(errorCaptor.capture());
    verify(taskProcessor, times(1)).process(successCaptor.capture());

    assert successCaptor.getValue().getTaskDto().getType().equals(TaskType.DOWNLOAD);
    assert successCaptor.getValue().getUrl().equals("http://localhost:8080");
    assert errorCaptor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getTaskState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getType().equals(TaskType.DOWNLOAD);
  }
}
