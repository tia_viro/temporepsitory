package handler;

import filler.TestFillerDataGenerator;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import reactor.core.publisher.Mono;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.params.UpdateTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.handler.UpdateTaskHandler;
import ru.x5.sm.job.task.processor.TaskProcessor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateTaskHandlerTest {
  @Mock
  private DaoStationService stationService;
  @Mock
  private DaoTaskService taskService;
  @Mock
  private TaskProcessor taskProcessor;
  @Mock
  private UpdateFileNameService updateFileNameService;
  @InjectMocks
  private UpdateTaskHandler updateTaskHandler;

  private JobDto job;
  private UpdateTaskUniqueParam taskCreationParam;
  private SMStationDto smStation;

  @Before
  public void before() {
    job = TestFillerDataGenerator.generateJobWithoutTargetNodes();
    taskCreationParam = new UpdateTaskUniqueParam();
    taskCreationParam.setTemplateVersions(TestFillerDataGenerator.generateTemplateVersionsByNodeGroupsDtos());
    smStation = TestFillerDataGenerator.generateSMStationDto();
  }

  @Test
  public void createTaskSuccessfullyTest() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> captor = ArgumentCaptor.forClass(TaskProcessorData.class);
    when(stationService.findStationDtoByName(any())).thenReturn(Mono.just(smStation));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    when(updateFileNameService.getFileNameForUpdate(any(), any())).thenReturn("package");
    updateTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskProcessor, times(2)).process(captor.capture());

    assert captor.getValue().getTaskDto().getType().equals(TaskType.UPDATE);
    assert captor.getValue().getUrl().equals("http://localhost:8080");
  }

  @Test
  public void createTaskWithErrorTest() throws NotFoundException {
    ArgumentCaptor<TaskDto> captor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName(any())).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    updateTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(2)).updateTask(captor.capture());

    assert captor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert captor.getValue().getTaskState().equals(TaskState.ERROR);
    assert captor.getValue().getType().equals(TaskType.UPDATE);
  }

  @Test
  public void createTaskMixedTest() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> successCaptor = ArgumentCaptor.forClass(TaskProcessorData.class);
    ArgumentCaptor<TaskDto> errorCaptor = ArgumentCaptor.forClass(TaskDto.class);
    when(stationService.findStationDtoByName(taskCreationParam.getTemplateVersions().get(0).getNodes().iterator().next())).thenReturn(Mono.just(smStation));
    when(stationService.findStationDtoByName(taskCreationParam.getTemplateVersions().get(1).getNodes().iterator().next())).thenThrow(new NotFoundException(""));
    when(taskService.save(any())).thenAnswer((Answer<Mono<TaskDto>>) invocation -> Mono.just((TaskDto) invocation.getArguments()[0]));
    when(updateFileNameService.getFileNameForUpdate(any(), any())).thenReturn("package");
    updateTaskHandler.createTask(job, taskCreationParam);
    verify(stationService, times(2)).findStationDtoByName(any());
    verify(taskService, times(2)).save(any());
    verify(taskService, times(1)).updateTask(errorCaptor.capture());
    verify(taskProcessor, times(1)).process(successCaptor.capture());

    assert successCaptor.getValue().getTaskDto().getType().equals(TaskType.UPDATE);
    assert successCaptor.getValue().getUrl().equals("http://localhost:8080");
    assert errorCaptor.getValue().getTaskStatuses().get(0).getState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getTaskState().equals(TaskState.ERROR);
    assert errorCaptor.getValue().getType().equals(TaskType.UPDATE);
  }
}
