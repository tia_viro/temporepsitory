package filler;

import ru.x5.sm.dto.job.*;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.structure.nodes.TemplateVersionsDto;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.dto.structure.station.SMStationTypeDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class TestFillerDataGenerator {
    public static JobDto generateJobWithoutTargetNodes() {
        JobDto job = new JobDto();
        JobPeriodDto period = new JobPeriodDto();
        period.setFrom(LocalDateTime.now());
        period.setTo(LocalDateTime.now().plusHours(2));
        job.setPeriod(period);
        return job;
    }

    public static JobDto generateJobWithTargetNodes() {
        JobDto job = new JobDto();
        JobPeriodDto period = new JobPeriodDto();
        period.setFrom(LocalDateTime.now());
        period.setTo(LocalDateTime.now().plusHours(2));
        job.setPeriod(period);
        return job;
    }

    public static List<TemplateVersionsByNodeGroupsDto> generateTemplateVersionsByNodeGroupsDtos() {
        TemplateVersionsByNodeGroupsDto firstTemplate = new TemplateVersionsByNodeGroupsDto();
        firstTemplate.setNodes(Set.of("firstNode"));
        firstTemplate.setSource("firstSource");
        firstTemplate.setTarget("XRG-BoServer");
        firstTemplate.setId(1L);

        TemplateVersionsByNodeGroupsDto secondTemplate = new TemplateVersionsByNodeGroupsDto();
        secondTemplate.setNodes(Set.of("secondNode"));
        secondTemplate.setSource("secondSource");
        secondTemplate.setTarget("XRG-BoServer");
        secondTemplate.setId(2L);
        return List.of(firstTemplate, secondTemplate);
    }

    public static SMStationDto generateSMStationDto() {
        SMStationDto smStation = new SMStationDto();
        smStation.setId(1L);
        smStation.setAddress("localhost");
        smStation.setPort(8080);
        smStation.setName("TEST");
        SMStationTypeDto stationTypeDto = new SMStationTypeDto();
        stationTypeDto.setName("XRG-BoServer");
        smStation.setType(stationTypeDto);
        return smStation;
    }

    public static StationDto generateStationDto() {
        StationDto station = new StationDto();
        station.setName("TEST");
        station.setStationId(1L);
        return station;
    }

    public static List<SelectedNodeDto> generateSelectedNodeDtos() {
        SelectedNodeDto firstSelectedNodeDto = new SelectedNodeDto();
        firstSelectedNodeDto.setNodeName("firstNode");
        firstSelectedNodeDto.setStationId(1L);

        SelectedNodeDto secondSelectedNodeDto = new SelectedNodeDto();
        secondSelectedNodeDto.setNodeName("secondNode");
        secondSelectedNodeDto.setStationId(2L);
        return List.of(firstSelectedNodeDto, secondSelectedNodeDto);
    }

    public static TemplateVersionsDto generateTemplateVersionsDto() {
        TemplateVersionsDto versionsDto = new TemplateVersionsDto();
        versionsDto.setCurrentVersion("currentVersion");
        versionsDto.setAvailableVersions(List.of("availableVersion"));
        return versionsDto;
    }

    public static DownloadJobDto generateDownloadJobWithTargetNodes() {
        DownloadJobDto job = new DownloadJobDto();
        JobPeriodDto period = new JobPeriodDto();
        period.setFrom(LocalDateTime.now());
        period.setTo(LocalDateTime.now().plusHours(2));
        job.setPeriod(period);
        job.setBandwidth(85);
        job.setDownloadTo("/");
        job.setPathsToFiles(Set.of("http://localhost"));
        job.setNexusLoad(true);
        job.setTargetSCNodes(Set.of("firstNode", "secondNode"));
        job.setType(JobType.DOWNLOAD);
        return job;
    }
}

