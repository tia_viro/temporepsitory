package service;

import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.UpdateTaskDto;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.TaskCancelServiceImpl;
import ru.x5.sm.job.service.TaskStateUpdateService;
import ru.x5.sm.job.task.processor.TaskProcessor;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TaskCancelServiceTest {
  @Mock
  private DaoStationService stationService;
  @Mock
  private TaskProcessor taskProcessor;
  @Mock
  private TaskStateUpdateService taskUpdateService;
  @InjectMocks
  private TaskCancelServiceImpl cancelService;

  private TaskDto task;
  private SMStationDto station;

  @Before
  public void before() {
    task = new UpdateTaskDto();
    task.setStationId(1L);
    task.setType(TaskType.DOWNLOAD);

    station = new SMStationDto();
    station.setAddress("http://localhost:8080");
  }

  @Test
  public void cancelTaskTest_StartedTask() throws NotFoundException {
    task.setTaskState(TaskState.STARTED);
    cancelService.cancelTask(task);
    verify(taskUpdateService, times(1)).updateStatus(task, TaskState.CANCELED);
    verify(stationService, never()).findStationDtoById(any());
    verify(taskProcessor, never()).process(any());
  }

  @Test
  public void cancelTaskTest_CancelUncanceledTask() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> taskProcessorDataCaptor =  ArgumentCaptor.forClass(TaskProcessorData.class);
    when(stationService.findStationDtoById(any())).thenReturn(Mono.just(station));
    task.setTaskState(TaskState.SENT);
    cancelService.cancelTask(task);
    verify(taskUpdateService, times(1)).updateStatus(task, TaskState.CANCELED);
    verify(stationService, times(1)).findStationDtoById(task.getStationId());
    verify(taskProcessor, times(1)).process(taskProcessorDataCaptor.capture());

    assert taskProcessorDataCaptor.getValue().getUrl().equals(station.getAddress());
    assert taskProcessorDataCaptor.getValue().getTaskURI().equals("/task/cancel");
  }

  @Test
  public void cancelTaskTest_CancelCanceledTask() throws NotFoundException {
    ArgumentCaptor<TaskProcessorData<?>> taskProcessorDataCaptor =  ArgumentCaptor.forClass(TaskProcessorData.class);
    when(stationService.findStationDtoById(any())).thenReturn(Mono.just(station));
    task.setTaskState(TaskState.CANCELED);
    cancelService.cancelTask(task);
    verify(taskUpdateService, never()).updateStatus(task, TaskState.CANCELED);
    verify(stationService, times(1)).findStationDtoById(task.getStationId());
    verify(taskProcessor, times(1)).process(taskProcessorDataCaptor.capture());

    assert taskProcessorDataCaptor.getValue().getUrl().equals(station.getAddress());
    assert taskProcessorDataCaptor.getValue().getTaskURI().equals("/task/cancel");
  }
}
