package service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.dto.structure.station.SMStationTypeDto;
import ru.x5.sm.job.service.UpdateFileNameService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class UpdateFileNameServiceTest {
  @Mock
  private Environment environment;
  private UpdateFileNameService service;

  @Before
  public void before() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    MockitoAnnotations.initMocks(this);
    when(environment.getProperty(any(String.class), eq("XRG-BoServer"))).thenReturn("XRG-BoServer");
    when(environment.getProperty(any(String.class), eq("XRG-SPOS"))).thenReturn("XRG-SPOS");
    when(environment.getProperty(any(String.class), eq("gk-boserver-{version}.unix.gkretail.update.tar.bz2"))).thenReturn("gk-boserver-{version}.unix.gkretail.update.tar.bz2");
    when(environment.getProperty(any(String.class), eq("gk-pos-{version}.unix.gkretail.update.tar.bz2"))).thenReturn("gk-pos-{version}.unix.gkretail.update.tar.bz2");
    service = new UpdateFileNameService(environment);
    Method postConstruct =  UpdateFileNameService.class.getDeclaredMethod("init",null);
    postConstruct.setAccessible(true);
    postConstruct.invoke(service);
  }

  @Test
  public void updateFileNameServiceTest() throws Exception {
    String file = service.getFileNameForUpdate("v12.08.47-SP25", createStation());
    assert file.equals("gk-boserver-12.08.47-SP25.unix.gkretail.update.tar.bz2");
  }

  private SMStationDto createStation() {
    final SMStationDto stationDto = new SMStationDto();
    final SMStationTypeDto stationTypeDto = new SMStationTypeDto();
    stationTypeDto.setName("XRG-BoServer-Franchise");
    stationTypeDto.setParentName("XRG-BoServer");
    stationDto.setType(stationTypeDto);
    return stationDto;
  }
}
