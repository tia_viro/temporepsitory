package loader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.UpdateTaskDto;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.loader.TaskLoader;
import ru.x5.sm.job.task.processor.TaskProcessor;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TaskLoaderTest {
  @Mock
  private DaoTaskService daoTaskService;
  @Mock
  private TaskProcessor taskProcessor;
  @Mock
  private DaoStationService stationService;
  @InjectMocks
  private TaskLoader taskLoader;

  private UpdateTaskDto task;
  private SMStationDto station;

  @Before
  public void before() {
    task = new UpdateTaskDto();
    task.setStationId(1L);
    task.setType(TaskType.DOWNLOAD);

    station = new SMStationDto();
    station.setAddress("http://localhost:8080");
  }

  @Test
  public void afterPropertiesSetTest() throws Exception {
    ArgumentCaptor<Long> stationIdCaptor = ArgumentCaptor.forClass(Long.class);
    ArgumentCaptor<TaskProcessorData<?>> taskProcessorDataCaptor = ArgumentCaptor.forClass(TaskProcessorData.class);
    when(daoTaskService.getAllNotSentTasks()).thenReturn(Flux.just(task));
    when(stationService.findStationDtoById(any())).thenReturn(Mono.just(station));
    taskLoader.afterPropertiesSet();
    verify(daoTaskService, times(1)).getAllNotSentTasks();
    verify(stationService, times(1)).findStationDtoById(stationIdCaptor.capture());
    verify(taskProcessor, times(1)).process(taskProcessorDataCaptor.capture());

    assert stationIdCaptor.getValue().equals(task.getStationId());
    assert taskProcessorDataCaptor.getValue().getTaskDto().equals(task);
    assert taskProcessorDataCaptor.getValue().getUrl().equals(station.getAddress());
    assert taskProcessorDataCaptor.getValue().getTaskURI().equals("/task/download");
  }
}
