package ru.x5.sm.job.task.handler;

import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.status.TaskStatusDto;
import ru.x5.sm.dto.job.status.TaskStepStatus;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.task.params.GenericTaskUniqueParam;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.task.processor.TaskProcessor;

public abstract class CommonTaskHandler<T extends TaskDto, C extends GenericTaskUniqueParam> implements TaskHandler<T, C> {
  protected final TaskProcessor taskProcessor;
  protected final DaoTaskService taskService;

  protected CommonTaskHandler(TaskProcessor taskProcessor, DaoTaskService taskService) {
    this.taskProcessor = taskProcessor;
    this.taskService = taskService;
  }

  protected void fillTaskDtoInfo(T task, SMStationDto station, JobDto job){
    task.setStationId(station.getId());
    task.setJobId(job.getId());
    task.setType(requiredTaskType());
    task.setValidFrom(job.getPeriod().getFrom());
    task.setValidTo(job.getPeriod().getTo());
    task.setTaskState(TaskState.STARTED);
  }

  protected void processData(T savedData, SMStationDto station){
    TaskProcessorData<T> data = new TaskProcessorData<>();
    data.setUrl("http://" + station.getAddress() + ":" + station.getPort());
    data.setTaskURI("/task/"+requiredTaskType().name().toLowerCase());
    data.setTaskDto(savedData);
    taskProcessor.process(data);
  }

  protected void saveOnSuccess(T task, SMStationDto station){
    taskService.save(task).subscribe(savedDto->processData((T) savedDto,station));
  }
  protected void saveOnError(String node,T task, JobDto job){
    task.setJobId(job.getId());
    task.setType(requiredTaskType());
    task.setTaskState(TaskState.ERROR);

    taskService.save(task).subscribe(savedData -> {
      TaskStatusDto status = new TaskStatusDto();
      status.setTaskId(savedData.getId());
      status.setState(TaskState.ERROR);
      status.setShortMessage(String.format("Can't find station %s to create task.", node));
      status.setStepStatus(TaskStepStatus.ERROR);
      savedData.getTaskStatuses().add(status);
      taskService.updateTask(savedData);
    });
  }

}
