package ru.x5.sm.job.task.factory.producer.utils;

import ru.x5.sm.dto.job.SelectedNodeDto;
import ru.x5.sm.dto.job.TemplateVersionsByNodeGroupsDto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ProducersUtils {
  public static List<TemplateVersionsByNodeGroupsDto> nodeFilter(List<TemplateVersionsByNodeGroupsDto> versions, Set<String> stations){
    return versions.stream()
        .filter(version->
            version.getNodes().stream()
                .filter(node->stations.contains(node))
                .findAny()
                .isPresent())
        .map(src->mapVersions(src,stations))
        .collect(Collectors.toList());
  }
  public static List<SelectedNodeDto> nodeFilterForRollback(List<SelectedNodeDto> selectedNodeDtos, Set<String> stations){
    return selectedNodeDtos.stream()
        .filter(nodeDto->stations.contains(nodeDto.getNodeName()))
        .collect(Collectors.toList());
  }

  public static TemplateVersionsByNodeGroupsDto mapVersions(TemplateVersionsByNodeGroupsDto versions,Set<String> stations){
    Set<String> nodes = versions.getNodes().stream().filter(src->stations.contains(src)).collect(Collectors.toSet());
    versions.setNodes(nodes);
    return versions;
  }
}
