package ru.x5.sm.job.task.factory.producer;

import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.RollbackJobDto;
import ru.x5.sm.dto.job.task.params.RollbackTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.producer.utils.ProducersUtils;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

@Component
public class RollbackJobRollbackTaskParamProducer implements TaskParamsProducer<RollbackJobDto, RollbackTaskUniqueParam> {
    @Override
    public RollbackTaskUniqueParam createParams(RollbackJobDto jobDto, JobRunnerDto jobRunnerDto) {
        RollbackTaskUniqueParam param = new RollbackTaskUniqueParam();
        param.setSelectedNodeDtos(ProducersUtils.nodeFilterForRollback(jobDto.getSelectedNodes(),jobRunnerDto.getStations()));
        return param;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.ROLLBACK;
    }

    @Override
    public JobType requireJobType() {
        return JobType.ROLLBACK;
    }
}
