package ru.x5.sm.job.task.factory.producer;

import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.UpdateJobDto;
import ru.x5.sm.dto.job.task.params.PrecheckTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.producer.utils.ProducersUtils;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

@Component
public class UpdateJobPrecheckTaskParamProducer implements TaskParamsProducer<UpdateJobDto, PrecheckTaskUniqueParam> {
    @Override
    public PrecheckTaskUniqueParam createParams(UpdateJobDto jobDto, JobRunnerDto jobRunnerDto) {
        PrecheckTaskUniqueParam param = new PrecheckTaskUniqueParam();
        param.setTemplateVersions(ProducersUtils.nodeFilter(jobDto.getTemplateVersions(),jobRunnerDto.getStations()));
        return param;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.PRECHECK;
    }

    @Override
    public JobType requireJobType() {
        return JobType.UPDATE;
    }
}
