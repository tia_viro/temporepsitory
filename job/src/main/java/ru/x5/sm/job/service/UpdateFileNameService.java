package ru.x5.sm.job.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ru.x5.sm.dto.structure.station.SMStationDto;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class UpdateFileNameService {
  private final Map<String, String> systemTypePatternMap;
  private final Environment env;

  public static final String DEFAULT_BO_TYPE = "XRG-BoServer";
  public static final String DEFAULT_POS_TYPE = "XRG-SPOS";
  public static final String DEFAULT_BO_FILE_PATTERN = "gk-boserver-{version}.unix.gkretail.update.tar.bz2";
  public static final String DEFAULT_POS_FILE_PATTERN = "gk-pos-{version}.unix.gkretail.update.tar.bz2";
  public static final String BO_FILE_PATTERN = "bo.update.file.pattern";
  public static final String POS_FILE_PATTERN = "pos.update.file.pattern";
  public static final String BO_SYSTEM_TYPE = "bo.system.type.name";
  public static final String POS_SYSTEM_TYPE = "pos.system.type.name";

  @PostConstruct
  private void init() {
    systemTypePatternMap.put(env.getProperty(BO_SYSTEM_TYPE, DEFAULT_BO_TYPE), env.getProperty(BO_FILE_PATTERN, DEFAULT_BO_FILE_PATTERN));
    systemTypePatternMap.put(env.getProperty(POS_SYSTEM_TYPE, DEFAULT_POS_TYPE), env.getProperty(POS_FILE_PATTERN, DEFAULT_POS_FILE_PATTERN));
  }

  public UpdateFileNameService(Environment env) {
    this.env = env;
    this.systemTypePatternMap = new HashMap<>();
  }

  public String getFileNameForUpdate(String version, SMStationDto station) {
    //    имя пакета формируем по родительскому типу (SPOS, BoServer)
    String stationType = getParentSystemType(station);

    String pattern = Optional.ofNullable(systemTypePatternMap.get(stationType))
      .orElseThrow(() -> new RuntimeException("Pattern not found for station type:" + stationType));

    return pattern.replaceAll("\\{version\\}", version.replaceAll("v",""));
  }

  public String getFileNameForTemplate(String version, SMStationDto station) {
    String stationType = getParentSystemType(station);
    return String.format("template-%s-%s.zip", stationType, version);
  }

  private String getParentSystemType(SMStationDto station) {
     return StringUtils.isNotEmpty(station.getType().getParentName()) ?
        station.getType().getParentName() : station.getType().getName();
  }
}
