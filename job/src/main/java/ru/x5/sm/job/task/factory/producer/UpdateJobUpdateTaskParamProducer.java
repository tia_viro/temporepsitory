package ru.x5.sm.job.task.factory.producer;

import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.TemplateVersionsByNodeGroupsDto;
import ru.x5.sm.dto.job.UpdateJobDto;
import ru.x5.sm.dto.job.task.params.UpdateTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.producer.utils.ProducersUtils;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UpdateJobUpdateTaskParamProducer implements TaskParamsProducer<UpdateJobDto, UpdateTaskUniqueParam> {
    @Override
    public UpdateTaskUniqueParam createParams(UpdateJobDto jobDto, JobRunnerDto jobRunnerDto) {
        UpdateTaskUniqueParam param = new UpdateTaskUniqueParam();
        param.setTemplateVersions(ProducersUtils.nodeFilter(jobDto.getTemplateVersions(),jobRunnerDto.getStations()));
        return param;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.UPDATE;
    }

    @Override
    public JobType requireJobType() {
        return JobType.UPDATE;
    }
}
