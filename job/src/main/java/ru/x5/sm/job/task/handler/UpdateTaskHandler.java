package ru.x5.sm.job.task.handler;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.TemplateVersionsByNodeGroupsDto;
import ru.x5.sm.dto.job.task.UpdateTaskDto;
import ru.x5.sm.dto.job.task.params.UpdateTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.processor.TaskProcessor;

@Component
public class UpdateTaskHandler extends CommonTaskHandler<UpdateTaskDto, UpdateTaskUniqueParam> {
  private final DaoStationService stationService;
  private final UpdateFileNameService updateFileNameService;

  public UpdateTaskHandler(DaoTaskService taskService, DaoStationService daoStationService, TaskProcessor taskProcessor, UpdateFileNameService updateFileNameService) {
    super(taskProcessor, taskService);
    this.stationService = daoStationService;
    this.updateFileNameService = updateFileNameService;
  }

  @Override
  public void createTask(JobDto job, UpdateTaskUniqueParam taskCreationDto) {
    taskCreationDto.getTemplateVersions().forEach(template -> navigateToNodes(template, job));
  }

  private void navigateToNodes(TemplateVersionsByNodeGroupsDto template, JobDto job) {
    template.getNodes().parallelStream()
      .forEach(node -> invokeTaskCreationOnStationReceived(node, template, job));
  }

  private void invokeTaskCreationOnStationReceived(String nodeName, TemplateVersionsByNodeGroupsDto template, JobDto job) {
    try {
      stationService.findStationDtoByName(nodeName)
        .subscribe(stationDto -> fillTaskProcessorData(stationDto, job, template));
    } catch (NotFoundException e) {
      fillErrorTask(nodeName, job);
    }
  }

  private void fillTaskProcessorData(SMStationDto station, JobDto job, TemplateVersionsByNodeGroupsDto template) {
    UpdateTaskDto task = new UpdateTaskDto();
    fillTaskDtoInfo(task, station, job);
    task.setNewVersion(template.getTarget());
    task.setOldVersion(template.getSource());
    task.setPackageName(updateFileNameService.getFileNameForUpdate(template.getTarget(), station));
    saveOnSuccess(task, station);
  }

  private void fillErrorTask(String node, JobDto job) {
    UpdateTaskDto task = new UpdateTaskDto();
    saveOnError(node,task,job);
  }

  @Override
  public TaskType requiredTaskType() {
        return TaskType.UPDATE;
    }
}
