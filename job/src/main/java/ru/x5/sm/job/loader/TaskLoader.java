package ru.x5.sm.job.loader;

import javassist.NotFoundException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.job.task.processor.TaskProcessor;

@Component
public class TaskLoader implements InitializingBean {
  private final DaoTaskService daoTaskService;
  private final TaskProcessor taskProcessor;
  private final DaoStationService stationService;

  public TaskLoader(DaoTaskService daoTaskService, TaskProcessor taskProcessor, DaoStationService stationService) {
    this.daoTaskService = daoTaskService;
    this.taskProcessor = taskProcessor;
    this.stationService = stationService;
  }

  private void loadTasks() {
    daoTaskService
      .getAllNotSentTasks()
      .subscribe(task -> {
        try {
          stationService.findStationDtoById(task.getStationId())
            .subscribe(station -> taskProcessor.process(new TaskProcessorData<>(task, station.getAddress(), String.format("/task/%s", task.getType().name().toLowerCase()))));
        } catch (NotFoundException e) {
//            TODO Add normal handling
        }
      });
    }

  @Override
  public void afterPropertiesSet() throws Exception {
    loadTasks();
  }
}
