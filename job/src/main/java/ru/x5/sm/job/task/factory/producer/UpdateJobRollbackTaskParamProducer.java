package ru.x5.sm.job.task.factory.producer;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.SelectedNodeDto;
import ru.x5.sm.dto.job.UpdateJobDto;
import ru.x5.sm.dto.job.task.params.RollbackTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.producer.utils.ProducersUtils;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

import java.util.List;

@Component
public class UpdateJobRollbackTaskParamProducer implements TaskParamsProducer<UpdateJobDto, RollbackTaskUniqueParam> {
    @Override
    public RollbackTaskUniqueParam createParams(UpdateJobDto jobDto, JobRunnerDto jobRunnerDto) {
        RollbackTaskUniqueParam param = new RollbackTaskUniqueParam();
        List<SelectedNodeDto> nodeDtoList = Lists.newArrayList();
        jobDto.getStores().parallelStream()
            .forEach(storeDto -> storeDto.getStations()
                .forEach(stationDto ->
                    nodeDtoList.add(new SelectedNodeDto(stationDto.getStationId(), stationDto.getName()))));
        param.setSelectedNodeDtos(ProducersUtils.nodeFilterForRollback(nodeDtoList,jobRunnerDto.getStations()));
        return param;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.ROLLBACK;
    }

    @Override
    public JobType requireJobType() {
        return JobType.UPDATE;
    }
}
