package ru.x5.sm.job.task.processor;

import reactor.core.publisher.FluxSink;
import reactor.core.publisher.WorkQueueProcessor;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.job.service.TaskCancelServiceImpl;
import ru.x5.sm.job.service.TaskStateUpdateService;
import ru.x5.sm.job.subscriber.TaskSubscriber;

public class TaskProcessorImpl implements TaskProcessor {
  private FluxSink<TaskProcessorData> sink;
  private WorkQueueProcessor<TaskProcessorData> processor;
  private TaskSubscriber taskSubscriber;

  public TaskProcessorImpl(TaskStateUpdateService taskStateUpdateService, DaoStationService stationService,
                           Integer bufferSize, Integer allowedFailNumber, Integer delay) {
    processor = WorkQueueProcessor.share("TaskQueue", bufferSize);
    taskSubscriber = new TaskSubscriber(this, taskStateUpdateService,
            new TaskCancelServiceImpl(stationService, this, taskStateUpdateService),
            allowedFailNumber, delay);
    processor
            .subscribe(taskSubscriber::process);
  }

  public void process(TaskProcessorData data) {
    processor.onNext(data);
  }
}
