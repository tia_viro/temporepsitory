package ru.x5.sm.job.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.job.service.TaskStateUpdateService;
import ru.x5.sm.job.task.processor.TaskProcessor;
import ru.x5.sm.job.task.processor.TaskProcessorImpl;

@Configuration
@ComponentScan("ru.x5.sm.job")
@PropertySource(value = "file:${config.dir:config}/task_sender.properties", ignoreResourceNotFound = true)
public class JobConfig {
  private static final String BUFFER_SIZE= "ru.x5.sm.tasksender.request.buffer.size";
  private static final String RESEND_DELAY= "ru.x5.sm.tasksender.request.delay";
  private static final String FAIL_COUNT= "ru.x5.sm.tasksender.request.failCount";
  private static final String DEFAULT_BUFFER_SIZE= "2048";//Значение переменной должно быть степенью 2
  private static final String DEFAULT_RESEND_DELAY= "1000";
  private static final String DEFAULT_FAIL_COUNT= "2048";

  private final Integer bufferSize;
  private final Integer resendDelay;
  private final Integer failCount;

  public JobConfig(Environment env) {
    this.bufferSize = Integer.valueOf(env.getProperty(BUFFER_SIZE, DEFAULT_BUFFER_SIZE));
    this.resendDelay = Integer.valueOf(env.getProperty(RESEND_DELAY, DEFAULT_RESEND_DELAY));
    this.failCount = Integer.valueOf(env.getProperty(FAIL_COUNT, DEFAULT_FAIL_COUNT));
  }

  @Bean
  public TaskProcessor taskProcessor(TaskStateUpdateService taskStateUpdateService, DaoStationService stationService) {
    return new TaskProcessorImpl(taskStateUpdateService, stationService, bufferSize, failCount, resendDelay);
  }
}
