package ru.x5.sm.job.task.factory.producer;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.UpdateJobDto;
import ru.x5.sm.dto.job.task.params.DownloadTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.producer.utils.ProducersUtils;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

@Component
@PropertySource(value = "file:${config.dir:config}/task_sender.properties", ignoreResourceNotFound = true)
public class UpdateJobDownloadTaskParamProducer implements TaskParamsProducer<UpdateJobDto, DownloadTaskUniqueParam> {
    private final Environment env;

    public UpdateJobDownloadTaskParamProducer(Environment environment) {
        this.env = environment;
    }

    @Override
    public DownloadTaskUniqueParam createParams(UpdateJobDto jobDto, JobRunnerDto jobRunnerDto) {
        DownloadTaskUniqueParam params = new DownloadTaskUniqueParam();
        params.setBandwidth(Integer.parseInt(env.getProperty(BANDWIDTH_SIZE, DEFAULT_BANDWIDTH_SIZE)));
        params.setTemplateVersions(ProducersUtils.nodeFilter(jobDto.getTemplateVersions(),jobRunnerDto.getStations()));
        return params;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.DOWNLOAD;
    }

    @Override
    public JobType requireJobType() {
        return JobType.UPDATE;
    }
}
