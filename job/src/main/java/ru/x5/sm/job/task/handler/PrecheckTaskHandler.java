package ru.x5.sm.job.task.handler;

import com.google.common.collect.Lists;
import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.StationDto;
import ru.x5.sm.dto.job.TemplateVersionsByNodeGroupsDto;
import ru.x5.sm.dto.job.task.PrecheckTaskDto;
import ru.x5.sm.dto.job.task.params.PrecheckTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.processor.TaskProcessor;

import java.util.List;

@Component
public class PrecheckTaskHandler extends CommonTaskHandler<PrecheckTaskDto, PrecheckTaskUniqueParam> {
  private final DaoStationService stationService;
  private final UpdateFileNameService updateFileNameService;

  public PrecheckTaskHandler(DaoStationService daoStationService, DaoTaskService taskService, TaskProcessor taskProcessor, UpdateFileNameService updateFileNameService) {
    super(taskProcessor, taskService);
    this.stationService = daoStationService;
    this.updateFileNameService = updateFileNameService;
  }

  @Override
  public void createTask(JobDto job, PrecheckTaskUniqueParam taskCreationDto) {
    List<Long> ids = Lists.newArrayList();
    job.getStores()
        .forEach(storeDto -> storeDto.getStations().stream()
            .filter(stationDto -> filterStation(stationDto,taskCreationDto))
            .forEach(stationDto -> ids.add(stationDto.getStationId())));
    super.taskService.deleteAllPreChecksForJob(job, ids);
    taskCreationDto.getTemplateVersions().forEach(template -> navigateToNodes(template, job));
  }
  private boolean filterStation(StationDto stationDto, PrecheckTaskUniqueParam taskCreationDto){
    return taskCreationDto.getTemplateVersions().stream()
        .anyMatch(versions -> versions.getNodes().contains(stationDto.getName()));
  }

  private void navigateToNodes(TemplateVersionsByNodeGroupsDto template, JobDto job) {
    template.getNodes().parallelStream()
        .forEach(node -> invokeTaskCreationOnStationReceived(node, job, template));
  }

  private void invokeTaskCreationOnStationReceived(String nodeName, JobDto job, TemplateVersionsByNodeGroupsDto template) {
    try {
      stationService.findStationDtoByName(nodeName)
          .subscribe(stationDto -> fillTaskProcessorData(stationDto, job, template));
    } catch (NotFoundException e) {
      fillErrorTask(nodeName, job);
    }
  }

  private void fillTaskProcessorData(SMStationDto station, JobDto job, TemplateVersionsByNodeGroupsDto template) {
    PrecheckTaskDto task = new PrecheckTaskDto();
    fillTaskDtoInfo(task, station, job);
    task.setNewVersion(template.getTarget());
    task.setPackageName(updateFileNameService.getFileNameForUpdate(template.getTarget(), station));
    saveOnSuccess(task, station);
  }

  private void fillErrorTask(String nodeName, JobDto job) {
    PrecheckTaskDto task = new PrecheckTaskDto();
    saveOnError(nodeName, task, job);
  }

  @Override
  public TaskType requiredTaskType() {
    return TaskType.PRECHECK;
  }
}
