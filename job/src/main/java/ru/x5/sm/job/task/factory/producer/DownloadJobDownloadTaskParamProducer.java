package ru.x5.sm.job.task.factory.producer;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.x5.sm.dto.job.DownloadJobDto;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.task.params.DownloadTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

import java.util.Collections;

@Component
@PropertySource(value = "file:${config.dir:config}/task_sender.properties", ignoreResourceNotFound = true)
public class DownloadJobDownloadTaskParamProducer implements TaskParamsProducer<DownloadJobDto, DownloadTaskUniqueParam> {
    private final Environment env;

    public DownloadJobDownloadTaskParamProducer(Environment environment) {
        this.env = environment;
    }

    @Override
    public DownloadTaskUniqueParam createParams(DownloadJobDto jobDto, JobRunnerDto jobRunnerDto) {
        DownloadTaskUniqueParam params = new DownloadTaskUniqueParam();
        params.setBandwidth(jobDto.getBandwidth());
        params.setPathToFile(jobDto.getPathsToFiles());
        params.setTemplateVersions(Collections.emptyList());
        return params;
    }

    @Override
    public TaskType requiredTaskType() {
        return TaskType.DOWNLOAD;
    }

    @Override
    public JobType requireJobType() {
        return JobType.DOWNLOAD;
    }
}
