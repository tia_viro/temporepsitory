package ru.x5.sm.job.service;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskDto;

@Component
public class TaskStateUpdateService {

  private DaoTaskService daoTaskService;

  public TaskStateUpdateService(DaoTaskService daoTaskService) {
    this.daoTaskService = daoTaskService;
  }

  public void updateStatus(TaskDto data, TaskState taskState) {
    // TODO подумать как оповещать через вебсокет
    daoTaskService.updateTaskState(data, taskState);
  }

  public Mono<Boolean> checkStatus(TaskState state, Long taskId) {
//    TODO Right realization
//    return daoTaskService.findTask(taskId)
//            .map(data -> data.getTaskDto().getTaskState().equals(state));
    return Mono.just(state != TaskState.CANCELED);
  }
}
