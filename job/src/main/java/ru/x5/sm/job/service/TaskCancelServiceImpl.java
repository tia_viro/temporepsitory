package ru.x5.sm.job.service;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.CancelTaskDto;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.processor.TaskProcessor;
import ru.x5.sm.job.task.service.TaskCancelService;

@Component
public class TaskCancelServiceImpl implements TaskCancelService {

  private final DaoStationService stationService;
  private final TaskProcessor taskProcessor;
  private final TaskStateUpdateService stateUpdateService;

  public TaskCancelServiceImpl(DaoStationService stationService, TaskProcessor taskProcessor, TaskStateUpdateService taskDtTaskStateUpdateService) {
    this.stationService = stationService;
    this.taskProcessor = taskProcessor;
    this.stateUpdateService = taskDtTaskStateUpdateService;
  }

  @Override
  public void cancelTask(TaskDto task) {
    if (task.getTaskState().equals(TaskState.STARTED)) {
      stateUpdateService.updateStatus(task, TaskState.CANCELED);
    } else {
      CancelTaskDto cancelTaskDto = new CancelTaskDto();
      cancelTaskDto.setType(TaskType.CANCEL);
      cancelTaskDto.setTaskIdForCancel(task.getId());
      try {
        stationService.findStationDtoById(task.getStationId())
          .subscribe(station -> taskProcessor.process(new TaskProcessorData<>(cancelTaskDto, station.getAddress(),
            String.format("/task/%s", cancelTaskDto.getType().name().toLowerCase()))));
      } catch (NotFoundException e) {
//                TODO Add normal handling
      }
      if (!task.getTaskState().equals(TaskState.CANCELED)) {
        stateUpdateService.updateStatus(task, TaskState.CANCELED);
      }
    }
  }
}
