package ru.x5.sm.job.task.handler;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.DownloadJobDto;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.TemplateVersionsByNodeGroupsDto;
import ru.x5.sm.dto.job.task.AuthType;
import ru.x5.sm.dto.job.task.CredentialsDto;
import ru.x5.sm.dto.job.task.DownloadTaskDto;
import ru.x5.sm.dto.job.task.params.DownloadTaskUniqueParam;
import ru.x5.sm.dto.job.type.JobType;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.processor.TaskProcessor;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DownloadTaskHandler extends CommonTaskHandler<DownloadTaskDto, DownloadTaskUniqueParam> {
  private final DaoStationService stationService;
  private final DownloadTaskHandlerDownloadJobScenario downloadJobScenarioHandler;
  private final DownloadTaskHandlerUpdateJobScenario updateJobScenarioHandler;
  private final UpdateFileNameService updateFileNameService;
  @Value("${nexus.login}")
  private String nexusLogin;
  @Value("${nexus.password}")
  private String nexusPassword;


  public DownloadTaskHandler(DaoStationService stationService, DaoTaskService taskService, TaskProcessor taskProcessor, UpdateFileNameService updateFileNameService) {
    super(taskProcessor,taskService);
    this.stationService = stationService;
    this.updateFileNameService = updateFileNameService;
    this.downloadJobScenarioHandler = new DownloadTaskHandlerDownloadJobScenario();
    this.updateJobScenarioHandler = new DownloadTaskHandlerUpdateJobScenario();
  }

  @Override
  public void createTask(JobDto job, DownloadTaskUniqueParam taskCreationParam) {
    if(job.getType().equals(JobType.UPDATE)){
      updateJobScenarioHandler.handle(job, taskCreationParam);
    } else if(job.getType().equals(JobType.DOWNLOAD)){
      downloadJobScenarioHandler.handle((DownloadJobDto) job, taskCreationParam);
    }
  }

  private DownloadTaskDto generateTaskDto(SMStationDto station, JobDto job, DownloadTaskUniqueParam taskCreationParam) {
    DownloadTaskDto task = new DownloadTaskDto();
    fillTaskDtoInfo(task,station,job);
    task.setSpeed(taskCreationParam.getBandwidth());
    return task;
  }
  private DownloadTaskDto generateTaskDto(SMStationDto station, DownloadJobDto job, DownloadTaskUniqueParam taskCreationParam) {
    DownloadTaskDto task = new DownloadTaskDto();
    fillTaskDtoInfo(task, station, job);
    task.setSpeed(taskCreationParam.getBandwidth());
    Map<String, String> fromTo = new HashMap<>();
    String downloadTo = job.getDownloadTo();
    taskCreationParam.getPathToFile().forEach(path -> fromTo.put(path, downloadTo));
    task.setFromTo(fromTo);
    task.setStationId(-1L);
    if (job.getNexusLoad() != null && job.getNexusLoad()) {
      CredentialsDto credentialsDto = new CredentialsDto(nexusLogin, nexusPassword, AuthType.BASIC);
      task.setCredentialsDto(credentialsDto);
    }
    return task;
  }

  private void fillErrorTask(String nodeName, JobDto job) {
    DownloadTaskDto task = new DownloadTaskDto();
    saveOnError(nodeName,task,job);
  }

  @Override
  public TaskType requiredTaskType() {
    return TaskType.DOWNLOAD;
  }


//  сценарий создания DownloadTask при исполнении Download job
  private class DownloadTaskHandlerDownloadJobScenario{
    private void handle(DownloadJobDto job, DownloadTaskUniqueParam taskCreationParam){
      job.getTargetSCNodes().parallelStream()
              .forEach(nodeName -> invokeTaskCreationOnStationReceived(nodeName, job, taskCreationParam));
    }

  private void invokeTaskCreationOnStationReceived(String nodeName, JobDto job, DownloadTaskUniqueParam taskCreationParam) {
    try {
      stationService.findStationDtoByName(nodeName)
              .subscribe(stationDto -> processTaskProcessorData(stationDto, job, taskCreationParam));
    } catch (NotFoundException e) {
      fillErrorTask(nodeName, job);
    }
  }

    private void processTaskProcessorData(SMStationDto station, JobDto job, DownloadTaskUniqueParam taskCreationParam) {
      DownloadTaskDto task = generateTaskDto(station, (DownloadJobDto) job, taskCreationParam);
      task.setFromTo(taskCreationParam.getPathToFile()
              .stream()
              .collect(Collectors.toMap(String::toString, String::toString)));
      saveOnSuccess(task,station);
    }
  }

//  сценарий создания DownloadTask при исполнении UpdateJob
  private class DownloadTaskHandlerUpdateJobScenario{
    private void handle(JobDto job, DownloadTaskUniqueParam taskCreationParam){
      taskCreationParam.getTemplateVersions()
              .forEach(template -> navigateToNodes(template, job, taskCreationParam));
    }

    private void navigateToNodes(TemplateVersionsByNodeGroupsDto template, JobDto job, DownloadTaskUniqueParam taskCreationParam) {
      template.getNodes().parallelStream()
             .forEach(node -> invokeTaskCreationOnStationReceived(node, template, job, taskCreationParam));
    }

    private void invokeTaskCreationOnStationReceived(String nodeName, TemplateVersionsByNodeGroupsDto template, JobDto job, DownloadTaskUniqueParam taskCreationParam) {
      try {
        stationService.findStationDtoByName(nodeName)
                .subscribe(stationDto -> processTaskProcessorData(stationDto, job, template, taskCreationParam));
      } catch (NotFoundException e) {
        fillErrorTask(nodeName, job);
      }
    }

    private void processTaskProcessorData(SMStationDto station, JobDto job, TemplateVersionsByNodeGroupsDto template,
                                          DownloadTaskUniqueParam taskCreationParam) {
      DownloadTaskDto task = generateTaskDto(station, job, taskCreationParam);
      Map<String, String> fromTo = new HashMap<>();
      String updatePackage = updateFileNameService.getFileNameForUpdate(template.getTarget(), station);
      fromTo.put(updatePackage, updatePackage);
//      String templateFile = updateFileNameService.getFileNameForTemplate(template.getTarget(), station);
//      fromTo.put(templateFile, templateFile);
      task.setFromTo(fromTo);
      task.setSpeed(85);
      saveOnSuccess(task,station);
    }
  }
}
