package ru.x5.sm.job.subscriber;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.x5.sm.dto.job.status.TaskState;
import ru.x5.sm.dto.job.task.TaskProcessorData;
import ru.x5.sm.job.service.TaskStateUpdateService;
import ru.x5.sm.job.task.processor.TaskProcessor;
import ru.x5.sm.job.task.service.TaskCancelService;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;
import static org.springframework.web.reactive.function.client.ClientResponse.create;
import static ru.x5.sm.dto.job.status.TaskState.SENDING;
import static ru.x5.sm.dto.job.status.TaskState.SENDING_FAILED;


public class TaskSubscriber {
  private static final Logger LOG = LogManager.getLogger();

  private TaskProcessor taskProcessor;
  private TaskStateUpdateService taskStateUpdateService;
  private TaskCancelService cancelService;
  private Integer allowedFailNumber;
  private Integer delay;

  public TaskSubscriber(TaskProcessor taskProcessor, TaskStateUpdateService taskStateUpdateService, TaskCancelService cancelService, Integer allowedFailNumber, Integer delay) {
    this.taskProcessor = taskProcessor;
    this.taskStateUpdateService = taskStateUpdateService;
    this.cancelService = cancelService;
    this.allowedFailNumber = allowedFailNumber;
    this.delay = delay;
  }

  public void process(TaskProcessorData data) {
    try {
      processData(data);
    } catch (Exception e) {
      LOG.error("Error happened during task processing: ", e);
    }
  }

  private void processData(TaskProcessorData data) {
    filter(data).subscribe(filterResult -> {
      if (filterResult) {
        getWebClient(data.getUrl())
                .post()
                .uri(data.getUrl() + data.getTaskURI())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(data.getTaskDto()))
                .exchange()
                .defaultIfEmpty(create(SERVICE_UNAVAILABLE).build())
                .onErrorReturn(create(SERVICE_UNAVAILABLE).build())
                .doOnSuccessOrError((response, throwable) -> finishProcessing(response, throwable, data))
                .block();
      }
    });
  }

  private WebClient getWebClient(String url) {
    return WebClient.builder()
            .baseUrl(url)
            .build();
  }

  private Mono<Boolean> filter(TaskProcessorData data) {
    return taskStateUpdateService.checkStatus(TaskState.CANCELED, data.getTaskDto().getId())
            .handle((checkResult, booleanSynchronousSink) -> {
              if (checkResult) {
                booleanSynchronousSink.next(false);
              } else {
                if (data.getTaskDto().getFailCount() == null) {
                  data.getTaskDto().setFailCount(0);
                  taskStateUpdateService.updateStatus(data.getTaskDto(), SENDING);
                }
                if (data.getTaskDto().getFailCount() < allowedFailNumber) {
                  booleanSynchronousSink.next(true);
                } else {
                  taskStateUpdateService.updateStatus(data.getTaskDto(), SENDING_FAILED);
                  booleanSynchronousSink.next(false);
                }
              }
            });
  }

  private void finishProcessing(ClientResponse response, Throwable throwable, TaskProcessorData data) {
    taskStateUpdateService.checkStatus(TaskState.CANCELED, data.getTaskDto().getId()).subscribe(checkResult -> {
      if (checkResult) {
        cancelService.cancelTask(data.getTaskDto());
      } else {
        if (throwable != null || !response.statusCode().equals(HttpStatus.OK)) {
          if (data.getTaskDto().getFailCount() == null) {
            data.getTaskDto().setFailCount(0);
            taskStateUpdateService.updateStatus(data.getTaskDto(), SENDING);
          }
          data.getTaskDto().setFailCount(data.getTaskDto().getFailCount() + 1);
          try {
            Thread.sleep(delay);
          } catch (InterruptedException ex) {
            taskProcessor.process(data);
          }
          taskProcessor.process(data);
        } else {
          taskStateUpdateService.updateStatus(data.getTaskDto(), TaskState.SENT);
        }
      }
    });
  }
}
