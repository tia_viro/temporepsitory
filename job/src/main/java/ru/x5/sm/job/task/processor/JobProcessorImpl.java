package ru.x5.sm.job.task.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.x5.sm.database.service.DaoJobService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.task.TaskDto;
import ru.x5.sm.dto.job.task.params.GenericTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.factory.UniqueTaskParamFactory;
import ru.x5.sm.job.task.handler.TaskHandler;

import java.util.List;

@Component
public class JobProcessorImpl implements JobProcessor {
  private final DaoJobService daoJobService;
  private final UniqueTaskParamFactory uniqueTaskParamFactory;

  private List<TaskHandler> taskHandlers;

  @Autowired
  public JobProcessorImpl(List<TaskHandler> taskHandlers, UniqueTaskParamFactory uniqueTaskParamFactory, DaoJobService daoJobService) {
    this.taskHandlers = taskHandlers;
    this.uniqueTaskParamFactory = uniqueTaskParamFactory;
    this.daoJobService = daoJobService;
  }

  @Override
  public void process(JobRunnerDto jobRunner) {
    daoJobService.findById(jobRunner.getJobId())
      //.filter(job -> job.getType().getPossibleTasks().contains(jobRunner.getTaskType()))
      //.switchIfEmpty(Mono.error(new IllegalArgumentException("Task and Job discrepancy")))
      .subscribe(job -> executeJob(job, jobRunner));
  }

  private void executeJob(JobDto job, JobRunnerDto jobRunnerDto) {
    taskHandlers.stream()
      .filter(taskHandler -> filterHandlers(jobRunnerDto.getTaskType(), taskHandler))
      .findFirst()
      .ifPresent(taskHandler -> handle(job, jobRunnerDto, taskHandler));
  }

  private boolean filterHandlers(TaskType taskType, TaskHandler handler) {
     return handler.requiredTaskType().equals(taskType);
  }

  private void handle(JobDto job, JobRunnerDto jobRunnerDto, TaskHandler<TaskDto, GenericTaskUniqueParam> handler) {
    handler.createTask(job, uniqueTaskParamFactory.generate(job, jobRunnerDto));
  }
}
