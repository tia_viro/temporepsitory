package ru.x5.sm.job.task.handler;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;

import ru.x5.sm.database.service.DaoSmService;
import ru.x5.sm.database.service.DaoStationService;
import ru.x5.sm.database.service.DaoTaskService;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.SelectedNodeDto;
import ru.x5.sm.dto.job.task.RollbackTaskDto;
import ru.x5.sm.dto.job.task.params.RollbackTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.dto.structure.station.SMStationDto;
import ru.x5.sm.job.service.UpdateFileNameService;
import ru.x5.sm.job.task.processor.TaskProcessor;

@Component
public class RollbackTaskHandler extends CommonTaskHandler<RollbackTaskDto, RollbackTaskUniqueParam> {
  private final DaoStationService stationService;
  private final UpdateFileNameService fileNameService;
  private final DaoSmService smService;

  public RollbackTaskHandler(DaoStationService stationService, DaoTaskService taskService, TaskProcessor taskProcessor, UpdateFileNameService fileNameService, DaoSmService smService) {
    super(taskProcessor, taskService);
    this.stationService = stationService;
    this.fileNameService = fileNameService;
    this.smService = smService;
  }

  @Override
  public void createTask(JobDto job, RollbackTaskUniqueParam taskCreationDto) {
    taskCreationDto.getSelectedNodeDtos().parallelStream()
        .forEach(node->invokeTaskCreationOnStationReceived(node,job));
  }


  private void invokeTaskCreationOnStationReceived(SelectedNodeDto selectedNodeDto, JobDto job) {
    try {
      stationService.findStationDtoByName(selectedNodeDto.getNodeName())
          .subscribe(stationDto -> fillTaskProcessorData(stationDto, job, selectedNodeDto));
    } catch (NotFoundException e) {
      fillErrorTask(selectedNodeDto.getNodeName(), job);
    }
  }

  private void fillTaskProcessorData(SMStationDto station, JobDto job, String version) {
    RollbackTaskDto task = new RollbackTaskDto();
    fillTaskDtoInfo(task, station, job);
    task.setOldVersion(version);
    task.setPackageName(fileNameService.getFileNameForUpdate(version, station));
    saveOnSuccess(task, station);
  }

  private void fillTaskProcessorData(SMStationDto station, JobDto job, SelectedNodeDto selectedNodeDto){
    smService.getVersions(selectedNodeDto.getStationId())
        .subscribe(templateVersionsDto ->
            fillTaskProcessorData(station,job,templateVersionsDto.getCurrentVersion()));
  }

  private void fillErrorTask(String nodeName, JobDto job) {
    RollbackTaskDto task = new RollbackTaskDto();
    saveOnError(nodeName,task,job);
  }

  @Override
  public TaskType requiredTaskType() {
    return TaskType.ROLLBACK;
  }
}
