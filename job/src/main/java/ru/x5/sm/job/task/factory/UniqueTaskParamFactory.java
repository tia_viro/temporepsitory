package ru.x5.sm.job.task.factory;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.x5.sm.dto.job.JobDto;
import ru.x5.sm.dto.job.JobRunnerDto;
import ru.x5.sm.dto.job.task.params.GenericTaskUniqueParam;
import ru.x5.sm.dto.job.type.TaskType;
import ru.x5.sm.job.task.producer.TaskParamsProducer;

import java.util.List;

@Component
public class UniqueTaskParamFactory {
    private final List<TaskParamsProducer> paramsCreationProducers;

    public UniqueTaskParamFactory(List<TaskParamsProducer> paramsCreationProducers) {
        this.paramsCreationProducers = paramsCreationProducers;
    }

    public GenericTaskUniqueParam generate(JobDto jobDto, JobRunnerDto jobRunnerDto) {
        return paramsCreationProducers.stream()
                .filter(taskParamsProducer -> taskParamsProducer.requireJobType().equals(jobDto.getType()))
                .filter(taskParamsProducer -> taskParamsProducer.requiredTaskType().equals(jobRunnerDto.getTaskType()))
                .map(taskParamsProducer -> taskParamsProducer.createParams(jobDto,jobRunnerDto))
                .findAny()
                .orElseThrow();

    }
}
