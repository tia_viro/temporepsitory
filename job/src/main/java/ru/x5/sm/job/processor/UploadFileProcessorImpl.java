package ru.x5.sm.job.processor;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;

import java.nio.file.Paths;

@Service
@PropertySource(value = "file:${config.dir:config}/ufs.properties", ignoreResourceNotFound = true)
public class UploadFileProcessorImpl implements UploadFileProcessor {
  private final static String UPLOAD_DIRECTORY = "sm.ufs.cache.dir";
  private final static String DEFAULT_UPLOAD_DIRECTORY = "/cache";

  private final String directory;

  public UploadFileProcessorImpl(Environment env) {
    this.directory = env.getProperty(UPLOAD_DIRECTORY, DEFAULT_UPLOAD_DIRECTORY);
  }

  @Override
  public void process(FilePart part) {
    part.transferTo(Paths.get(String.format("%s/%s", directory, part.filename())))
      .subscribe();
  }
}
